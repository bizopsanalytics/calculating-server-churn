-- calculate sens where cross-grading could have been the reason for churn
with    
land_sen as (
        select  sen, 
                base_product,
                email_domain,
                quarter,
                min(financial_year) as financial_year,
                min(date) as min_date
        from    public.sale
        where   license_level= 'Full'
        and     platform = 'Server'
        and     base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and     financial_year > 'FY2010'
        and     financial_year <> 'FY2017'
        group by 1,2,3,4
),
renewed as (
        select  a.sen,
                a.email_domain,
                b.quarter,
                max(b.date) as max_date
        from    land_sen as a 
        left join public.sale as b on a.sen = b.sen
        where   b.sale_type = 'Renewal'
        and     b.quarter > a.quarter
        group by 1,2,3
        order by 1
),
land_churn_date as (
        select  sen, 
                (min_date + interval '545' DAY) as churn_date
        from land_sen
        where sen not in (select sen from renewed) 
),
renewed_churn_date as (
        select  sen, 
                (max_date + interval '545' DAY) as churn_date
        from    renewed
)
,non_renewed_prod_count as (      
        select  a.sen,
                a.email_domain,
                b.base_product,
                count(distinct b.sen) as prod_count
        from    land_sen as a
        left join public.license as b on a.email_domain = b.tech_email_domain
        left join land_churn_date as c on a.sen = c.sen
        where   a.sen in (select sen from land_churn_date)
        and     cast(b.start_date as date) between a.min_date and c.churn_date
        and     b.platform = 'Server'
        and     b.license_level = 'Full'
        group by 1,2,3
)      
,renewed_prod_count as (      
        select  a.sen,
                a.email_domain,
                b.base_product,
                count(distinct b.sen) as prod_count
        from    land_sen as a
        left join public.license as b on a.email_domain = b.tech_email_domain
        left join renewed_churn_date as c on a.sen = c.sen
        where   a.sen in (select sen from renewed_churn_date)
        and     cast(b.start_date as date) between a.min_date and c.churn_date
        and     b.platform = 'Server'
        and     b.license_level = 'Full'
        group by 1,2,3      
),
final_multi_sen_list as (
select  a.financial_year, 
        a.sen,
        a.base_product as land_product,
        b.base_product as cross_product
        from land_sen as a
        left join non_renewed_prod_count as b on a.sen = b.sen
   
union 

select  a.financial_year, 
        a.sen,
        a.base_product as land_product,
        b.base_product as cross_product
        from land_sen as a
        left join renewed_prod_count as b on a.sen = b.sen
)
select  a.financial_year,
        a.land_product,
        a.cross_product,  
        count(distinct a.sen)
from final_multi_sen_list as a
group by 1,2,3
order by 1,2,3