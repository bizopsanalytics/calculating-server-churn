-- find remaining sens
Drop table if exists playground_bizops.scr_remaining;
Create table playground_bizops.scr_remaining as
        select  sen, 
                min(financial_year) as financial_year,
                min(date) as min_date
        from    public.sale
        where   license_level= 'Full'
        and     platform = 'Server'
        and     base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and     financial_year > 'FY2010'
        and     financial_year <> 'FY2017'
        group by 1;

select  a.sen, a.financial_year  
from playground_bizops.scr_remaining as a
left join license as b on a.sen = cast(b.sen as text)
where   a.sen not in (select sen from playground_bizops.scr_consolidation)
and     a.sen not in (select sen from playground_bizops.scr_migration)
and     a.sen not in (select sen from playground_bizops.scr_downgrade)
and     a.sen not in (select sen from playground_bizops.scr_crossgrade)
and     b.expiry_date <= '2016-06-30' --ensure the license is not active