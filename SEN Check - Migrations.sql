with joiner as (
select server_sen
from playground_finance.Server_to_Cloud_JIRA_migrations
where license_level = 'Full' 

union 

select server_sen 
from playground_finance.Server_to_Cloud_nonJIRA_migrations
where license_level = 'Full' 

),
list as (
select  a.server_sen,
        min(b.financial_year) as land_year
from joiner as a
left join sale as b on cast(a.server_sen as text) = b.sen
group by 1
)
select land_year, count(distinct server_sen) as migrate_count
from list
group by 1