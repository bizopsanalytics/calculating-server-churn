WITH churn_buckets AS
(
SELECT *, 
CASE 
WHEN stage_reason IN(
'Company Out of Business', 
'Consolidating Instances',
'Cross-graded to JIRA Core', 
'Downgrade to Starter',
'Using another instance (same product)',
'Moved to Cloud',
'JIRA Renaissance Package / Pricing',
'Moved to another BTF product',
'Moved to Data Center' ) THEN 'Non-regretted'
WHEN stage_reason IN(
'Don''t want to renew', 
'No longer use Product', 
'No need for Support / Updates',
'Too expensive / No budget',
'Using Competitor''s Product',
'Waiting to Update Product',
'Not satisfied with Support / Updates',
'Waiting to Upgrade User-Tiers',
'Outstanding support issues / feature requests',
'Not in Production' ) THEN 'Regretted'
WHEN stage_reason IN(
'Contact info unavailable' ,
'Invalid Contact Information',
'Invalid Opportunity',
'No Response',
'Reason not given',
'Unable to Contact', 
'Project Completed',
'Other/no additional information given',
'Company Out of Business',
'Other (see General Comments)' ) OR stage_reason is null THEN 'Exclude'
END AS churn_type
FROM playground_intsys.renewals_lost
WHERE lost_date >= '2015-07-01'
),
b AS (
SELECT sen,
CASE 
WHEN amount BETWEEN 0 and 499 THEN 'Less than $500'
WHEN amount BETWEEN 500 and 999 THEN 'Between $500 and $999'
WHEN amount BETWEEN 1000 and 4999 THEN 'Between $1k and $5k'
WHEN amount BETWEEN 5000 and 9999 THEN 'Between $5k and $10k'
WHEN amount >= 10000 THEN 'Greater than $10k' END AS value_bucket,
stage_reason
FROM churn_buckets
GROUP BY 1,2,3
ORDER BY 2)
SELECT stage_reason, count(sen)
FROM b
--where sen in (
    --    select  a.sen 
      --  from playground_bizops.scr_downgrade_country180 as a
      --  left join license as b on a.sen = cast(b.sen as text)
        --where   a.sen not in (select sen from playground_bizops.scr_consolidation)
        --and     a.sen not in (select sen from playground_bizops.scr_migration)
        --and     a.sen not in (select sen from playground_bizops.scr_downgrade)
        --and     a.sen not in (select sen from playground_bizops.scr_crossgrade)
      --  and     b.expiry_date <= '2016-06-30' --ensure the license is not active
--)
--group by 1
--order by 2 desc

where sen in (

)
group by 1
order by 2 desc