
with    
land_sen as (
        select  sen,
                quarter,
                financial_year, 
                min(date) as min_date
        from    public.sale
        where   license_level= 'Full'
        and     platform = 'Server'
        and     base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and     financial_year > 'FY2011'
        group by 1,2,3
        ),
renewed as (
        select  a.sen 
        from    land_sen as a 
        left join public.sale as b on a.sen = b.sen
        where   b.sale_type = 'Renewal'
        and     a.min_date < b.date
        group by 1
        order by 1
        )
select a.financial_year, count(distinct a.sen), count(distinct b.sen)
from land_sen as a 
left join renewed as b on a.sen = b.sen
group by 1
order by 1


