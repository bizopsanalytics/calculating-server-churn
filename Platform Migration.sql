with    
land_sen as (
        select  sen, 
                base_product,
                email_domain,
                quarter,
                financial_year,
                max(user_limit) as land_tier,
                min(date) as min_date
        from    public.sale
        where   license_level= 'Full'
        and     platform = 'Server'
        and     base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and     financial_year > 'FY2010'
        and     financial_year <> 'FY2017'
        group by 1,2,3,4,5
)

select
        l.base_product,
        date_month(existing_end_date) as "Month",
        count(distinct case when l.license_level='Full' then t.sen else null end) as "Full Server-to-Cloud Migration"
from license l
inner join
(
 select
        lr.tech_email,
        lr.tech_email_domain,
        lr.existing_user_limit,
        existing_end_date, l.sen,
        lr.tech_region,
        lr.base_product
 from license_renewal lr
 inner join license l on (lr.sen = l.sen::text)
 left join land_sen as b on l.sen = b.sen
 where  1 = 1
        and l.base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and existing_end_date between date_trunc('month',current_date) - interval '13 months' and date_trunc('month',current_date) - interval '1 day'--'1 month 1 day'
        and lr.platform='Server'
        and l.license_level in ('Full')
        and not renewed
) t
on (
        l.tech_email_domain = t.tech_email_domain
        and l.tech_region = t.tech_region
        and l.base_product = t.base_product
        and l.purchase_date between existing_end_date - 90 and existing_end_date + 90
        and l.user_limit >= t.existing_user_limit)
  
where   1 = 1
        and l.base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and l.platform = 'Cloud'
        and l.license_level in ('Full')
        and l.purchase_date > date_trunc('month',current_date) - interval '16 months'
group by 1,2
order by 1,2;