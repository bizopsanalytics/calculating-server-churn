--all product Server migrations to Cloud
--create table for further analysis
Drop Table if exists zone_bizops.dc_migrations;
create table zone_bizops.dc_migrations as
--
with migrations as (
select
        l.base_product,
        month(existing_end_date) as "Month",
        l.last_paid_license_level,
        t.sen as server_sen
from public.license l
inner join
(
 select
        lr.tech_email,
        lr.tech_email_domain,
        lr.existing_user_limit,
        existing_end_date, l.sen, 
        lr.tech_region,
        lr.base_product
 from public.license_renewal lr
 inner join public.license l on lr.sen = l.sen
 where  1 = 1
        --and l.base_product not in ('JIRA', 'JIRA Software')
        and existing_end_date between date_trunc('month',current_date) - interval '61' month and date_trunc('month',current_date) - interval '1' day--'1 month 1 day'
        and lr.platform='Server'
        and l.last_paid_license_level in ('Full')
        and not renewed
) t
on (
        l.tech_email_domain = t.tech_email_domain
        and l.tech_region = t.tech_region
        and l.base_product = t.base_product
        and cast(l.paid_license_start_date as date) between existing_end_date - interval '90' day and existing_end_date + interval '90' day
        and l.last_paid_user_limit >= t.existing_user_limit)
  
where   1 = 1
        and l.platform = 'Data Center' --'Cloud'
        and l.last_paid_license_level in ('Full')
        and cast(l.paid_license_start_date as date) > date_trunc('month',current_date) - interval '64' month
)
select  a.server_sen as sen,
        min(b.financial_year) as land_year
from migrations as a
left join public.sale as b on a.server_sen = b.sen
where b.financial_year not in ('FY2010', 'FY2017')
group by 1

;
Drop Table if exists zone_bizops.cloud_migration;
create table zone_bizops.cloud_migration as

with migrations as (
select
        l.base_product,
        month(existing_end_date) as "Month",
        l.last_paid_license_level,
        t.sen as server_sen
from public.license l
inner join
(
 select
        lr.tech_email,
        lr.tech_email_domain,
        lr.existing_user_limit,
        existing_end_date, l.sen, 
        lr.tech_region,
        lr.base_product
 from public.license_renewal lr
 inner join public.license l on lr.sen = l.sen
 where  1 = 1
        and existing_end_date between date_trunc('month',current_date) - interval '61' month and date_trunc('month',current_date) - interval '1' day--'1 month 1 day'
        and lr.platform='Server'
        and l.last_paid_license_level in ('Full')
        and not renewed
) t
on (
        l.tech_email_domain = t.tech_email_domain
        and l.tech_region = t.tech_region
        and l.base_product = t.base_product
        and cast(l.paid_license_start_date as date) between existing_end_date - interval '90' day and existing_end_date + interval '90' day
        and l.last_paid_user_limit >= t.existing_user_limit)
  
where   1 = 1
        and l.platform = 'Cloud'
        and l.last_paid_license_level in ('Full')
        and cast(l.paid_license_start_date as date) > date_trunc('month',current_date) - interval '64' month
)
select  a.server_sen as sen,
        min(b.financial_year) as land_year
from migrations as a
left join public.sale as b on a.server_sen = b.sen
where b.financial_year not in ('FY2010', 'FY2017')
group by 1;

select * from zone_bizops.dc_migrations;
select * from zone_bizops.cloud_migration;


--sensitivities 180 day

with migrations as (
select
        l.base_product,
        month(existing_end_date) as "Month",
        l.last_paid_license_level,
        t.sen as server_sen
from public.license l
inner join
(
 select
        lr.tech_email,
        lr.tech_email_domain,
        lr.existing_user_limit,
        existing_end_date, l.sen, 
        lr.tech_region,
        lr.base_product
 from public.license_renewal lr
 inner join public.license l on lr.sen = l.sen
 where  1 = 1
        --and l.base_product not in ('JIRA', 'JIRA Software')
        and existing_end_date between date_trunc('month',current_date) - interval '61' month and date_trunc('month',current_date) - interval '1' day--'1 month 1 day'
        and lr.platform='Server'
        and l.last_paid_license_level in ('Full')
        and not renewed
) t
on (
        l.tech_email_domain = t.tech_email_domain
        and l.tech_region = t.tech_region
        and l.base_product = t.base_product
        and cast(l.paid_license_start_date as date) between existing_end_date - interval '180' day and existing_end_date + interval '180' day
        and l.last_paid_user_limit >= t.existing_user_limit)
  
where   1 = 1
        and l.platform = 'Data Center' --'Cloud'
        and l.last_paid_license_level in ('Full')
        and cast(l.paid_license_start_date as date) > date_trunc('month',current_date) - interval '64' month
)
select  b.financial_year as land_year,
        count(a.server_sen) as sen
from migrations as a
left join public.sale as b on a.server_sen = b.sen
where b.financial_year not in ('FY2010', 'FY2017')
group by 1
order by 1
;

with migrations as (
select
        l.base_product,
        month(existing_end_date) as "Month",
        l.last_paid_license_level,
        t.sen as server_sen
from public.license l
inner join
(
 select
        lr.tech_email,
        lr.tech_email_domain,
        lr.existing_user_limit,
        existing_end_date, l.sen, 
        lr.tech_region,
        lr.base_product
 from public.license_renewal lr
 inner join public.license l on lr.sen = l.sen
 where  1 = 1
        and existing_end_date between date_trunc('month',current_date) - interval '61' month and date_trunc('month',current_date) - interval '1' day--'1 month 1 day'
        and lr.platform='Server'
        and l.last_paid_license_level in ('Full')
        and not renewed
) t
on (
        l.tech_email_domain = t.tech_email_domain
        and l.tech_region = t.tech_region
        and l.base_product = t.base_product
        and cast(l.paid_license_start_date as date) between existing_end_date - interval '180' day and existing_end_date + interval '180' day
        and l.last_paid_user_limit >= t.existing_user_limit)
  
where   1 = 1
        and l.platform = 'Cloud'
        and l.last_paid_license_level in ('Full')
        and cast(l.paid_license_start_date as date) > date_trunc('month',current_date) - interval '64' month
)
select  b.financial_year as land_year,
        count(a.server_sen) as sen
from migrations as a
left join public.sale as b on a.server_sen = b.sen
where b.financial_year not in ('FY2010', 'FY2017')
group by 1
order by 1;

--sensitivities domain only + 90

with migrations as (
select
        l.base_product,
        month(existing_end_date) as "Month",
        l.last_paid_license_level,
        t.sen as server_sen
from public.license l
inner join
(
 select
        lr.tech_email,
        lr.tech_email_domain,
        lr.existing_user_limit,
        existing_end_date, l.sen, 
        --lr.tech_region,
        lr.base_product
 from public.license_renewal lr
 inner join public.license l on lr.sen = l.sen
 where  1 = 1
        --and l.base_product not in ('JIRA', 'JIRA Software')
        and existing_end_date between date_trunc('month',current_date) - interval '61' month and date_trunc('month',current_date) - interval '1' day--'1 month 1 day'
        and lr.platform='Server'
        and l.last_paid_license_level in ('Full')
        and not renewed
) t
on (
        l.tech_email_domain = t.tech_email_domain
        --and l.tech_region = t.tech_region
        and l.base_product = t.base_product
        and cast(l.paid_license_start_date as date) between existing_end_date - interval '90' day and existing_end_date + interval '90' day
        and l.last_paid_user_limit >= t.existing_user_limit)
  
where   1 = 1
        and l.platform = 'Data Center' --'Cloud'
        and l.last_paid_license_level in ('Full')
        and cast(l.paid_license_start_date as date) > date_trunc('month',current_date) - interval '64' month
)
select  b.financial_year as land_year,
        count(a.server_sen) as sen
from migrations as a
left join public.sale as b on a.server_sen = b.sen
where b.financial_year not in ('FY2010', 'FY2017')
group by 1
order by 1
;

with migrations as (
select
        l.base_product,
        month(existing_end_date) as "Month",
        l.last_paid_license_level,
        t.sen as server_sen
from public.license l
inner join
(
 select
        lr.tech_email,
        lr.tech_email_domain,
        lr.existing_user_limit,
        existing_end_date, l.sen, 
        --lr.tech_region,
        lr.base_product
 from public.license_renewal lr
 inner join public.license l on lr.sen = l.sen
 where  1 = 1
        and existing_end_date between date_trunc('month',current_date) - interval '61' month and date_trunc('month',current_date) - interval '1' day--'1 month 1 day'
        and lr.platform='Server'
        and l.last_paid_license_level in ('Full')
        and not renewed
) t
on (
        l.tech_email_domain = t.tech_email_domain
        --and l.tech_region = t.tech_region
        and l.base_product = t.base_product
        and cast(l.paid_license_start_date as date) between existing_end_date - interval '90' day and existing_end_date + interval '90' day
        and l.last_paid_user_limit >= t.existing_user_limit)
  
where   1 = 1
        and l.platform = 'Cloud'
        and l.last_paid_license_level in ('Full')
        and cast(l.paid_license_start_date as date) > date_trunc('month',current_date) - interval '64' month
)
select  b.financial_year as land_year,
        count(a.server_sen) as sen
from migrations as a
left join public.sale as b on a.server_sen = b.sen
where b.financial_year not in ('FY2010', 'FY2017')
group by 1
order by 1;

--sensitivities domain only + 180

with migrations as (
select
        l.base_product,
        month(existing_end_date) as "Month",
        l.last_paid_license_level,
        t.sen as server_sen
from public.license l
inner join
(
 select
        lr.tech_email,
        lr.tech_email_domain,
        lr.existing_user_limit,
        existing_end_date, l.sen, 
        --lr.tech_region,
        lr.base_product
 from public.license_renewal lr
 inner join public.license l on lr.sen = l.sen
 where  1 = 1
        --and l.base_product not in ('JIRA', 'JIRA Software')
        and existing_end_date between date_trunc('month',current_date) - interval '61' month and date_trunc('month',current_date) - interval '1' day--'1 month 1 day'
        and lr.platform='Server'
        and l.last_paid_license_level in ('Full')
        and not renewed
) t
on (
        l.tech_email_domain = t.tech_email_domain
        --and l.tech_region = t.tech_region
        and l.base_product = t.base_product
        and cast(l.paid_license_start_date as date) between existing_end_date - interval '180' day and existing_end_date + interval '180' day
        and l.last_paid_user_limit >= t.existing_user_limit)
  
where   1 = 1
        and l.platform = 'Data Center' --'Cloud'
        and l.last_paid_license_level in ('Full')
        and cast(l.paid_license_start_date as date) > date_trunc('month',current_date) - interval '64' month
)
select  b.financial_year as land_year,
        count(a.server_sen) as sen
from migrations as a
left join public.sale as b on a.server_sen = b.sen
where b.financial_year not in ('FY2010', 'FY2017')
group by 1
order by 1
;

with migrations as (
select
        l.base_product,
        month(existing_end_date) as "Month",
        l.last_paid_license_level,
        t.sen as server_sen
from public.license l
inner join
(
 select
        lr.tech_email,
        lr.tech_email_domain,
        lr.existing_user_limit,
        existing_end_date, l.sen, 
        --lr.tech_region,
        lr.base_product
 from public.license_renewal lr
 inner join public.license l on lr.sen = l.sen
 where  1 = 1
        and existing_end_date between date_trunc('month',current_date) - interval '61' month and date_trunc('month',current_date) - interval '1' day--'1 month 1 day'
        and lr.platform='Server'
        and l.last_paid_license_level in ('Full')
        and not renewed
) t
on (
        l.tech_email_domain = t.tech_email_domain
        --and l.tech_region = t.tech_region
        and l.base_product = t.base_product
        and cast(l.paid_license_start_date as date) between existing_end_date - interval '180' day and existing_end_date + interval '180' day
        and l.last_paid_user_limit >= t.existing_user_limit)
  
where   1 = 1
        and l.platform = 'Cloud'
        and l.last_paid_license_level in ('Full')
        and cast(l.paid_license_start_date as date) > date_trunc('month',current_date) - interval '64' month
)
select  b.financial_year as land_year,
        count(a.server_sen) as sen
from migrations as a
left join public.sale as b on a.server_sen = b.sen
where b.financial_year not in ('FY2010', 'FY2017')
group by 1
order by 1;