-- calculate sens where consolidation could have been the reason for churn
with    
land_sen as 
        (--identify the financial year that the sen was created in
        select  sen, 
                base_product,
                email_domain,
                quarter,
                financial_year,
                min(date) as min_date
        from    public.sale
        where   license_level= 'Full'
        and     platform = 'Server'
        and     base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and     financial_year > 'FY2010'
        and     financial_year <> 'FY2017'
        group by 1,2,3,4,5
),
renewed as 
        (--identify when a particular SEN was renewed last
        select  a.sen,
                a.email_domain,
                b.quarter,
                max(b.date) as max_date
        from    land_sen as a 
        left join public.sale as b on a.sen = b.sen
        where   b.sale_type = 'Renewal'
        and     b.quarter > a.quarter
        group by 1,2,3
        order by 1
),
land_churn_date as 
        (--if a sen never renewed, find that latest date that they could have renewed their license - 180 days after 1 year.
        select  sen, 
                (min_date + 545) as churn_date
        from land_sen
        where sen not in (select sen from renewed) 
),
renewed_churn_date as 
        (--if a sen did renew, find the date they should've renewed again - 180 days after last renew + 1 year.
        select  sen, 
                (max_date + 545) as churn_date
        from    renewed
)
,non_renewed_prod_count as 
        (--check the never-renewed group for how many of each base product they own between their land date and expected renew date.      
        select  a.sen,
                a.email_domain,
                b.base_product,
                count(distinct b.sen) as prod_count
        from land_sen as a
        left join public.license as b on a.email_domain = b.tech_email_domain
        left join land_churn_date as c on a.sen = c.sen
        where a.sen in (select sen from land_churn_date)
        and b.start_date between a.min_date and c.churn_date
        and a.base_product = b.base_product
        and b.platform = 'Server'
        group by 1,2,3
)      
,renewed_prod_count as 
        (--check the renewed group for how many of each base product they own between their latest renew date and expected next renew date.      
        select  a.sen,
                a.email_domain,
                b.base_product,
                count(distinct b.sen) as prod_count
        from land_sen as a
        left join public.license as b on a.email_domain = b.tech_email_domain
        left join renewed_churn_date as c on a.sen = c.sen
        where a.sen in (select sen from renewed_churn_date)
        and b.start_date between a.min_date and c.churn_date
        and a.base_product = b.base_product
        and b.platform = 'Server'
        group by 1,2,3      
),
final_multi_sen_list as 
        (--join the never-renewed and renewed group together for more analysis
        select  a.financial_year, 
        a.sen
        from land_sen as a
        left join non_renewed_prod_count as b on a.sen = b.sen
        where b.prod_count > 1
        
union 

        select  a.financial_year, 
        a.sen
        from land_sen as a
        left join renewed_prod_count as b on a.sen = b.sen
        where b.prod_count > 1
        )
        -- give a summary result by the year the customer first landed the sen.
        select  a.financial_year,  
                count(distinct a.sen)
        from    final_multi_sen_list as a
        left join license as b on a.sen = cast(b.sen as text)
        where   b.expiry_date <= '2016-06-30'
        group by 1