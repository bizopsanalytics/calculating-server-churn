--identify the number of times a Starter license is created for the same base_product after its maximum renewal date.
with    
land_sen as (
        select  sen, 
                base_product,
                email_domain,
                quarter,
                financial_year,
                min(date) as min_date
        from    public.sale
        where   license_level= 'Full'
        and     platform = 'Server'
        and     base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and     financial_year > 'FY2010'
        group by 1,2,3,4,5
        ),
renewed as (
        select  a.sen,
                a.email_domain, 
                b.quarter,
                b.financial_year,
                max(b.date) as max_date
        from    land_sen as a 
        left join public.sale as b on a.sen = b.sen
        where   b.sale_type = 'Renewal'
        and     b.quarter > a.quarter
        group by 1,2,3,4
        order by 1
        ),
starter_land as (
        select  sen, 
                email_domain,
                base_product,
                quarter,
                min(date) as min_start
        from public.sale
        where sale_type = 'Starter'
        and platform = 'Server'
        group by 1,2,3,4
        ),
renewed_starter as (
        select  a.quarter as land_quarter,
                c.quarter as max_renew_quarter,
                a.financial_year,
                b.quarter as starter_land_quarter,
                a.sen as affected_sen,
                a.email_domain
from    land_sen as a
left join starter_land as b on a.email_domain = b.email_domain
left join renewed as c on a.sen = c.sen
where   a.base_product = b.base_product
and     b.min_start > c.max_date + interval '275' DAY
and     b.min_start < c.max_date + interval '455' DAY
and     a.quarter < c.quarter
group by 1,2,3,4,5,6
order by 1,2,3
),
non_renewed_starter as
(
select  a.quarter as land_quarter,
        a.financial_year,
        b.quarter as starter_land_quarter,
        a.sen as affected_sen,
        a.email_domain
from    land_sen as a
left join starter_land as b on a.email_domain = b.email_domain
where   a.base_product = b.base_product
and     b.min_start > a.min_date + interval '275' DAY
and     b.min_start < a.min_date + interval '455' DAY
and     a.sen not in (select sen from renewed)
group by 1,2,3,4,5
order by 1,2
),
downgrade_samesen as
(
select          a.financial_year, 
                a.sen,
                a.email_domain
                from land_sen as a 
                left join public.sale as b on a.sen = b.sen
                where b.platform = 'Server'
                and b.license_level = 'Starter'
                and b.date >= a.min_date
                group by 1,2,3
),
final_set as (
        select  a.financial_year, 
                a.affected_sen as renewed, 
                a.email_domain as renew_email,
                b.affected_sen as non_renewed,
                b.email_domain as non_email
        from renewed_starter as a
        join non_renewed_starter as b on a.financial_year = b.financial_year
        and a.affected_sen not in (select sen from downgrade_samesen)
        and b.affected_sen not in (select sen from downgrade_samesen)
        group by 1,2,3,4,5
        order by 1
)
        select  a.financial_year,
                c.company_size,
                d.base_product,
                d.last_user_limit,
                a.renew_email,
                a.renewed
        from final_set as a
        left join zone_bizops.customer_size as c on a.renew_email = c.email_domain
        left join public.license as d on a.renewed = d.sen
        where c.company_size = 20000
        group by 1,2,3,4,5,6
        order by 1,2,3,4,5,6
