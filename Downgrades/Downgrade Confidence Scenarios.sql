-- Not renew but get new sen of same product
--confidence when country is used - 90 days
with    
land_sen as (
        select  sen, 
                base_product,
                email_domain,
                country,
                quarter,
                financial_year,
                min(date) as min_date
        from    public.sale
        where   license_level= 'Full'
        and     platform = 'Server'
        and     base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and     financial_year > 'FY2010'
        group by 1,2,3,4,5,6
        ),
downgrade_samesen as (
select          a.financial_year, 
                a.sen,
                a.email_domain
                from land_sen as a 
                left join public.sale as b on a.sen = b.sen
                where b.platform = 'Server'
                and b.license_level = 'Starter'
                and b.date >= a.min_date
group by 1,2,3
),
renewed as (
        select  a.sen,
                a.email_domain,
                b.quarter,
                b.financial_year,
                max(b.date) as max_date
        from    land_sen as a 
        left join public.sale as b on a.sen = b.sen
        where   b.sale_type = 'Renewal'
        and     b.quarter > a.quarter
        group by 1,2,3,4
        order by 1
        ),
starter_land as (
        select  sen, 
                email_domain,
                country,
                base_product,
                quarter,
                min(date) as min_start
        from public.sale
        where sale_type = 'Starter'
        and platform = 'Server'
        group by 1,2,3,4,5
        ),
renewed_starter as (
        select  a.quarter as land_quarter,
                c.quarter as max_renew_quarter,
                a.financial_year,
                a.email_domain,
                a.country,
                b.quarter as starter_land_quarter,
                a.sen as affected_sen
from    land_sen as a
left join starter_land as b on a.email_domain = b.email_domain and a.country = b.country
left join renewed as c on a.sen = c.sen
where   a.base_product = b.base_product
and     b.min_start > c.max_date + interval '275' DAY
and     b.min_start < c.max_date + interval '455' DAY
and     a.quarter < c.quarter
group by 1,2,3,4,5,6,7
order by 1,2,3
),
non_renewed_starter as
(
select  a.quarter as land_quarter,
        a.financial_year,
        b.quarter as starter_land_quarter,
        a.sen as affected_sen,
        a.email_domain,
        a.country
from    land_sen as a
left join starter_land as b on a.email_domain = b.email_domain and a.country = b.country
where   a.base_product = b.base_product
and     b.min_start > a.min_date + interval '275' DAY
and     b.min_start < a.min_date + interval '455' DAY
and     a.sen not in (select sen from renewed)
group by 1,2,3,4,5,6
order by 1,2
),
active_prod as (
select  a.email_domain,
        a.country,
        b.base_product,
        count(distinct b.sen) as prod_count
from    land_sen as a
left join public.license as b on a.email_domain = b.tech_email_domain and a.country = b.tech_country
left join starter_land as c on a.email_domain = c.email_domain and a.country = c.country and a.base_product = c.base_product
where   b.last_license_level = 'Full'
and     cast(b.paid_license_end_date as date) < c.min_start
and     cast(b.paid_license_end_date as date) > c.min_start - interval '90' DAY
group by 1,2,3
                )
select  a.financial_year,
        a.affected_sen
from    renewed_starter as a
join    active_prod as c on a.email_domain = c.email_domain and a.country = c.country
and     a.affected_sen not in (select sen from downgrade_samesen)
and     c.prod_count = 1

union

select  b.financial_year,
        b.affected_sen
from    non_renewed_starter as b 
join    active_prod as c on b.email_domain = c.email_domain and b.country = c.country
and     b.affected_sen not in (select sen from downgrade_samesen)
and     c.prod_count = 1
group by 1,2
order by 1;

--confidence when country is used - 180 days
with    
land_sen as (
        select  sen, 
                base_product,
                email_domain,
                country,
                quarter,
                financial_year,
                min(date) as min_date
        from    public.sale
        where   license_level= 'Full'
        and     platform = 'Server'
        and     base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and     financial_year > 'FY2010'
        group by 1,2,3,4,5,6
        ),
downgrade_samesen as (
select          a.financial_year, 
                a.sen,
                a.email_domain
                from land_sen as a 
                left join public.sale as b on a.sen = b.sen
                where b.platform = 'Server'
                and b.license_level = 'Starter'
                and b.date >= a.min_date
group by 1,2,3
),
renewed as (
        select  a.sen,
                a.email_domain,
                b.quarter,
                b.financial_year,
                max(b.date) as max_date
        from    land_sen as a 
        left join public.sale as b on a.sen = b.sen
        where   b.sale_type = 'Renewal'
        and     b.quarter > a.quarter
        group by 1,2,3,4
        order by 1
        ),
starter_land as (
        select  sen, 
                email_domain,
                country,
                base_product,
                quarter,
                min(date) as min_start
        from public.sale
        where sale_type = 'Starter'
        and platform = 'Server'
        group by 1,2,3,4,5
        ),
renewed_starter as (
        select  a.quarter as land_quarter,
                c.quarter as max_renew_quarter,
                a.financial_year,
                a.email_domain,
                a.country,
                b.quarter as starter_land_quarter,
                a.sen as affected_sen
from    land_sen as a
left join starter_land as b on a.email_domain = b.email_domain and a.country = b.country
left join renewed as c on a.sen = c.sen
where   a.base_product = b.base_product
and     b.min_start > c.max_date + interval '180' DAY
and     b.min_start < c.max_date + interval '545' DAY
and     a.quarter < c.quarter
group by 1,2,3,4,5,6,7
order by 1,2,3
),
non_renewed_starter as
(
select  a.quarter as land_quarter,
        a.financial_year,
        b.quarter as starter_land_quarter,
        a.sen as affected_sen,
        a.email_domain,
        a.country
from    land_sen as a
left join starter_land as b on a.email_domain = b.email_domain and a.country = b.country
where   a.base_product = b.base_product
and     b.min_start > a.min_date + interval '180' DAY
and     b.min_start < a.min_date + interval '545' DAY
and     a.sen not in (select sen from renewed)
group by 1,2,3,4,5,6
order by 1,2
),
active_prod as (
select  a.email_domain,
        a.country,
        b.base_product,
        count(distinct b.sen) as prod_count
from    land_sen as a
left join public.license as b on a.email_domain = b.tech_email_domain and a.country = b.tech_country
left join starter_land as c on a.email_domain = c.email_domain and a.country = c.country and a.base_product = c.base_product
where   b.last_license_level = 'Full'
and     cast(b.paid_license_end_date as date) < c.min_start
and     cast(b.paid_license_end_date as date) > c.min_start - interval '180' DAY
group by 1,2,3
                )
select  a.financial_year,
        a.affected_sen
from    renewed_starter as a
join    active_prod as c on a.email_domain = c.email_domain and a.country = c.country
and     a.affected_sen not in (select sen from downgrade_samesen)
and     c.prod_count = 1

union

select  b.financial_year,
        b.affected_sen
from    non_renewed_starter as b 
join    active_prod as c on b.email_domain = c.email_domain and b.country = c.country
and     b.affected_sen not in (select sen from downgrade_samesen)
and     c.prod_count = 1
group by 1,2
order by 1;

--confidence when Domain only +/-180 days
with    
land_sen as (
        select  sen, 
                base_product,
                email_domain,
                quarter,
                financial_year,
                min(date) as min_date
        from    public.sale
        where   license_level= 'Full'
        and     platform = 'Server'
        and     base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and     financial_year > 'FY2010'
        group by 1,2,3,4,5
        ),
downgrade_samesen as (
select          a.financial_year, 
                a.sen,
                a.email_domain
                from land_sen as a 
                left join public.sale as b on a.sen = b.sen
                where b.platform = 'Server'
                and b.license_level = 'Starter'
                and b.date >= a.min_date
group by 1,2,3
),
renewed as (
        select  a.sen,
                a.email_domain,
                b.quarter,
                b.financial_year,
                max(b.date) as max_date
        from    land_sen as a 
        left join public.sale as b on a.sen = b.sen
        where   b.sale_type = 'Renewal'
        and     b.quarter > a.quarter
        group by 1,2,3,4
        order by 1
        ),
starter_land as (
        select  sen, 
                email_domain,
                base_product,
                quarter,
                min(date) as min_start
        from public.sale
        where sale_type = 'Starter'
        and platform = 'Server'
        group by 1,2,3,4
        ),
renewed_starter as (
        select  a.quarter as land_quarter,
                c.quarter as max_renew_quarter,
                a.financial_year,
                a.email_domain,
                b.quarter as starter_land_quarter,
                a.sen as affected_sen
from    land_sen as a
left join starter_land as b on a.email_domain = b.email_domain
left join renewed as c on a.sen = c.sen
where   a.base_product = b.base_product
and     b.min_start > c.max_date + interval '180' DAY
and     b.min_start < c.max_date + interval '545' DAY
and     a.quarter < c.quarter
group by 1,2,3,4,5,6
order by 1,2,3
),
non_renewed_starter as
(
select  a.quarter as land_quarter,
        a.financial_year,
        b.quarter as starter_land_quarter,
        a.sen as affected_sen,
        a.email_domain
from    land_sen as a
left join starter_land as b on a.email_domain = b.email_domain
where   a.base_product = b.base_product
and     b.min_start > a.min_date + interval '180' DAY
and     b.min_start < a.min_date + interval '545' DAY
and     a.sen not in (select sen from renewed)
group by 1,2,3,4,5
order by 1,2
),
active_prod as (
select  a.email_domain,
        b.base_product,
        count(distinct b.sen) as prod_count
from    land_sen as a
left join public.license as b on a.email_domain = b.tech_email_domain 
left join starter_land as c on a.email_domain = c.email_domain  and a.base_product = c.base_product
where   b.last_license_level = 'Full'
and     cast(b.paid_license_end_date as date) < c.min_start
and     cast(b.paid_license_end_date as date) > c.min_start - interval '180' DAY
group by 1,2
                )
select  a.financial_year,
        a.affected_sen
from    renewed_starter as a
join    active_prod as c on a.email_domain = c.email_domain
and     a.affected_sen not in (select sen from downgrade_samesen)
and     c.prod_count = 1

union

select  b.financial_year,
        b.affected_sen
from    non_renewed_starter as b 
join    active_prod as c on b.email_domain = c.email_domain 
and     b.affected_sen not in (select sen from downgrade_samesen)
and     c.prod_count = 1
group by 1,2
order by 1;