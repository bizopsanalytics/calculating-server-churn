--all product Server migrations to Cloud
with migrations as (
select
        l.base_product,
        date_month(existing_end_date) as "Month",
        l.license_level,
        t.sen as server_sen
from license l
inner join
(
 select
        lr.tech_email,
        lr.tech_email_domain,
        lr.existing_user_limit,
        existing_end_date, l.sen, 
        lr.tech_region,
        lr.base_product
 from license_renewal lr
 inner join license l on (lr.sen = l.sen::text)
 where  1 = 1
        --and l.base_product not in ('JIRA', 'JIRA Software')
        and existing_end_date between date_trunc('month',current_date) - interval '61 months' and date_trunc('month',current_date) - interval '1 day'--'1 month 1 day'
        and lr.platform='Server'
        and l.license_level in ('Full')
        and not renewed
) t
on (
        l.tech_email_domain = t.tech_email_domain
        and l.tech_region = t.tech_region
        and l.base_product = t.base_product
        and l.purchase_date between existing_end_date - 90 and existing_end_date + 90
        and l.user_limit >= t.existing_user_limit)
  
where   1 = 1
        --and l.base_product not in ('JIRA', 'JIRA Software')
        and l.platform = 'Data Center' --'Cloud'
        and l.license_level in ('Full')
        and l.purchase_date > date_trunc('month',current_date) - interval '64 months'
--group by 1,2
--order by 1,2
),
list as (
select  a.server_sen,
        min(b.financial_year) as land_year
from migrations as a
left join sale as b on cast(a.server_sen as text) = b.sen
group by 1
)
select  land_year, 
        count(distinct server_sen) as migrate_count
from list
group by 1
