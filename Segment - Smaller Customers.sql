
--sens of small customers
with land_year as
(
        select  a.email_domain, 
                max(b.company_size) as company_size,
                min(a.date) as min_date,
                min(a.financial_year) as land_FY
        from    public.sale as a
        left join playground_bizops.customer_size as b on a.email_domain = b.email_domain
        -- left join zone_bizops.customer_size as b on a.email_domain = b.email_domain --socrates version
        where   sale_type = 'New to New'
        and     b.company_size <=200
        and     a.platform = 'Server'
        and     a.financial_year <> 'FY2017'
        group by 1
)
select  b.sen       
from    land_year as a
left join public.sale as b on a.email_domain = b.email_domain
where   a.land_fy <= b.financial_year
and     b.financial_year <> 'FY2017'
group by 1
order by 1

