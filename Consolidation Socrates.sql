-- calculate sens where consolidation could have been the reason for churn

--create a table to store the results for later consolidation.
Drop Table if exists zone_bizops.consolidation;
Create Table zone_bizops.consolidation as

--create temporary table that contains the first date of each particular SEN.
with    
land_sen as (
        select  sen, 
                base_product,
                email_domain,
                quarter,
                financial_year,
                min(date) as min_date
        from    public.sale
        where   license_level= 'Full'
        and     platform = 'Server'
        and     base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and     financial_year > 'FY2010'
        and     financial_year <> 'FY2017'
        group by 1,2,3,4,5
),
renewed as (
        select  a.sen,
                a.email_domain,
                b.quarter,
                max(b.date) as max_date
        from    land_sen as a 
        left join public.sale as b on a.sen = b.sen
        where   b.sale_type = 'Renewal'
        and     b.quarter > a.quarter
        group by 1,2,3
        order by 1
),
land_churn_date as (
        select  sen, 
                (min_date + interval '545' day) as churn_date
        from land_sen
        where sen not in (select sen from renewed) 
),
renewed_churn_date as (
        select  sen, 
                (max_date + interval '545' day) as churn_date
        from    renewed
)
,non_renewed_prod_count as (      
        select  a.sen,
                a.email_domain,
                b.base_product,
                count(distinct b.sen) as prod_count
        from land_sen as a
        left join public.license as b on a.email_domain = b.tech_email_domain
        left join land_churn_date as c on a.sen = c.sen
        where a.sen in (select sen from land_churn_date)
        and cast(b.paid_license_start_date as date) between a.min_date and c.churn_date
        and a.base_product = b.base_product
        and b.platform = 'Server'
        group by 1,2,3
)      
,renewed_prod_count as (      
        select  a.sen,
                a.email_domain,
                b.base_product,
                count(distinct b.sen) as prod_count
        from land_sen as a
        left join public.license as b on a.email_domain = b.tech_email_domain
        left join renewed_churn_date as c on a.sen = c.sen
        where a.sen in (select sen from renewed_churn_date)
        and cast(b.paid_license_start_date as date) between a.min_date and c.churn_date
        and a.base_product = b.base_product
        and b.platform = 'Server'
        group by 1,2,3      
),
final_multi_sen_list as (
select  a.financial_year, 
        a.sen
        from land_sen as a
        left join non_renewed_prod_count as b on a.sen = b.sen
        where b.prod_count > 1
        
union 

select  a.financial_year, 
        a.sen
        from land_sen as a
        left join renewed_prod_count as b on a.sen = b.sen
        where b.prod_count > 1
)
--for the sen count enable this
        --select  a.financial_year,  
        --a.sen
        --from final_multi_sen_list as a
        --left join public.license as b on a.sen = b.sen
        --where b.paid_license_end_date <= '2016-06-30'
        --group by 1,2;

--for the summary enable this
        select  a.financial_year,  
        count(a.sen)
        from final_multi_sen_list as a
        left join public.license as b on a.sen = b.sen
        where b.paid_license_end_date <= '2016-06-30'
        group by 1;
        
--check sensitivities against country
with    
land_sen as (
        select  sen, 
                base_product,
                email_domain,
                country,
                quarter,
                financial_year,
                min(date) as min_date
        from    public.sale
        where   license_level= 'Full'
        and     platform = 'Server'
        and     base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and     financial_year > 'FY2010'
        and     financial_year <> 'FY2017'
        group by 1,2,3,4,5,6
),
renewed as (
        select  a.sen,
                a.email_domain,
                b.quarter,
                max(b.date) as max_date
        from    land_sen as a 
        left join public.sale as b on a.sen = b.sen
        where   b.sale_type = 'Renewal'
        and     b.quarter > a.quarter
        group by 1,2,3
        order by 1
),
land_churn_date as (
        select  sen, 
                (min_date + interval '545' day) as churn_date
        from land_sen
        where sen not in (select sen from renewed) 
),
renewed_churn_date as (
        select  sen, 
                (max_date + interval '545' day) as churn_date
        from    renewed
)
,non_renewed_prod_count as (      
        select  a.sen,
                a.email_domain,
                b.base_product,
                count(distinct b.sen) as prod_count
        from land_sen as a
        left join public.license as b on a.email_domain = b.tech_email_domain and a.country = b.tech_country
        left join land_churn_date as c on a.sen = c.sen
        where a.sen in (select sen from land_churn_date)
        and cast(b.paid_license_start_date as date) between a.min_date and c.churn_date
        and a.base_product = b.base_product
        and b.platform = 'Server'
        group by 1,2,3
)      
,renewed_prod_count as (      
        select  a.sen,
                a.email_domain,
                b.base_product,
                count(distinct b.sen) as prod_count
        from land_sen as a
        left join public.license as b on a.email_domain = b.tech_email_domain and a.country = b.tech_country
        left join renewed_churn_date as c on a.sen = c.sen
        where a.sen in (select sen from renewed_churn_date)
        and cast(b.paid_license_start_date as date) between a.min_date and c.churn_date
        and a.base_product = b.base_product
        and b.platform = 'Server'
        group by 1,2,3      
),
final_multi_sen_list as (
select  a.financial_year, 
        a.sen
        from land_sen as a
        left join non_renewed_prod_count as b on a.sen = b.sen
        where b.prod_count > 1
        
union 

select  a.financial_year, 
        a.sen
        from land_sen as a
        left join renewed_prod_count as b on a.sen = b.sen
        where b.prod_count > 1
)
--for the summary
select  a.financial_year,  
        count(a.sen)
from final_multi_sen_list as a
left join public.license as b on a.sen = b.sen
where b.paid_license_end_date <= '2016-06-30'
group by 1;

        
--check sensitivities against tech email
with    
land_sen as (
        select  sen, 
                base_product,
                email_domain,
                email,
                country,
                quarter,
                financial_year,
                min(date) as min_date
        from    public.sale
        where   license_level= 'Full'
        and     platform = 'Server'
        and     base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and     financial_year > 'FY2010'
        and     financial_year <> 'FY2017'
        group by 1,2,3,4,5,6,7
),
renewed as (
        select  a.sen,
                a.email_domain,
                b.quarter,
                max(b.date) as max_date
        from    land_sen as a 
        left join public.sale as b on a.sen = b.sen
        where   b.sale_type = 'Renewal'
        and     b.quarter > a.quarter
        group by 1,2,3
        order by 1
),
land_churn_date as (
        select  sen, 
                (min_date + interval '545' day) as churn_date
        from land_sen
        where sen not in (select sen from renewed) 
),
renewed_churn_date as (
        select  sen, 
                (max_date + interval '545' day) as churn_date
        from    renewed
)
,non_renewed_prod_count as (      
        select  a.sen,
                a.email_domain,
                b.base_product,
                count(distinct b.sen) as prod_count
        from land_sen as a
        left join public.license as b on a.email_domain = b.tech_email_domain and a.email = b.tech_email
        left join land_churn_date as c on a.sen = c.sen
        where a.sen in (select sen from land_churn_date)
        and cast(b.paid_license_start_date as date) between a.min_date and c.churn_date
        and a.base_product = b.base_product
        and b.platform = 'Server'
        group by 1,2,3
)      
,renewed_prod_count as (      
        select  a.sen,
                a.email_domain,
                b.base_product,
                count(distinct b.sen) as prod_count
        from land_sen as a
        left join public.license as b on a.email_domain = b.tech_email_domain and a.email = b.tech_email
        left join renewed_churn_date as c on a.sen = c.sen
        where a.sen in (select sen from renewed_churn_date)
        and cast(b.paid_license_start_date as date) between a.min_date and c.churn_date
        and a.base_product = b.base_product
        and b.platform = 'Server'
        group by 1,2,3      
),
final_multi_sen_list as (
select  a.financial_year, 
        a.sen
        from land_sen as a
        left join non_renewed_prod_count as b on a.sen = b.sen
        where b.prod_count > 1
        
union 

select  a.financial_year, 
        a.sen
        from land_sen as a
        left join renewed_prod_count as b on a.sen = b.sen
        where b.prod_count > 1
)
--for the summary
select  a.financial_year,  
        count(a.sen)
from final_multi_sen_list as a
left join public.license as b on a.sen = b.sen
where b.paid_license_end_date <= '2016-06-30'
group by 1;

--select * from zone_bizops.consolidation
