drop table if exists playground_bizops.scr_actual_churn_JulFY16;
create table playground_bizops.scr_actual_churn_JulFY16 as

        with opening as 
        (
        select date_id, a.license_id, b.sen
        from fact_license_active as a
        left join dim_license as b on a.license_id = b.license_id
        left join dim_product as c on a.product_id = c.product_id
        where a.date_id in 
               (20150630, 
                20150731, 
                20150831, 
                20150930, 
                20151031, 
                20151130, 
                20151231, 
                20160131, 
                20160229, 
                20160331, 
                20160430, 
                20160531, 
                20160630)
        and b.level = 'Full'
        and c.base_product in 
                ('JIRA',
                'JIRA Core',
                'JIRA Software',
                'JIRA Service Desk',
                'Confluence',
                'HipChat',
                'Bitbucket',
                'Bamboo',
                'FishEye',
                'Crucible'
                )
        and c.platform = 'Server'
        group by 1,2,3
        )      
        
                        select  license_id, sen
                        from opening 
                        where date_id = 20150630
                        and license_id not in (select license_id from opening where date_id = 20150731)
                    ;    
drop table if exists playground_bizops.scr_actual_churn_AugFY16;
create table playground_bizops.scr_actual_churn_AugFY16 as

        with opening as 
        (
        select date_id, a.license_id, b.sen
        from fact_license_active as a
        left join dim_license as b on a.license_id = b.license_id
        left join dim_product as c on a.product_id = c.product_id
        where a.date_id in 
               (20150630, 
                20150731, 
                20150831, 
                20150930, 
                20151031, 
                20151130, 
                20151231, 
                20160131, 
                20160229, 
                20160331, 
                20160430, 
                20160531, 
                20160630)
        and b.level = 'Full'
        and c.base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and c.platform = 'Server'
        group by 1,2,3
        )      
        
                        select  license_id, sen
                        from opening 
                        where date_id = 20150731
                        and license_id not in (select license_id from opening where date_id = 20150831)
                        
                        ;
                        
drop table if exists playground_bizops.scr_actual_churn_SepFY16;
create table playground_bizops.scr_actual_churn_SepFY16 as

        with opening as 
        (
        select date_id, a.license_id, b.sen
        from fact_license_active as a
        left join dim_license as b on a.license_id = b.license_id
        left join dim_product as c on a.product_id = c.product_id
        where a.date_id in 
               (20150630, 
                20150731, 
                20150831, 
                20150930, 
                20151031, 
                20151130, 
                20151231, 
                20160131, 
                20160229, 
                20160331, 
                20160430, 
                20160531, 
                20160630)
        and b.level = 'Full'
        and c.base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and c.platform = 'Server'
        group by 1,2,3
        )      
                        select license_id, sen
                        from opening 
                        where date_id = 20150831
                        and license_id not in (select license_id from opening where date_id = 20150930)
                        
                        ;
                        
drop table if exists playground_bizops.scr_actual_churn_OctFY16;
create table playground_bizops.scr_actual_churn_OctFY16 as

        with opening as 
        (
        select date_id, a.license_id, b.sen
        from fact_license_active as a
        left join dim_license as b on a.license_id = b.license_id
        left join dim_product as c on a.product_id = c.product_id
        where a.date_id in 
               (20150630, 
                20150731, 
                20150831, 
                20150930, 
                20151031, 
                20151130, 
                20151231, 
                20160131, 
                20160229, 
                20160331, 
                20160430, 
                20160531, 
                20160630)
        and b.level = 'Full'
        and c.base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and c.platform = 'Server'
        group by 1,2,3
        )      
                        select license_id, sen
                        from opening 
                        where date_id = 20150930
                        and license_id not in (select license_id from opening where date_id = 20151031)
                
                ;
                
drop table if exists playground_bizops.scr_actual_churn_NovFY16;
create table playground_bizops.scr_actual_churn_NovFY16 as

        with opening as 
        (
        select date_id, a.license_id, b.sen
        from fact_license_active as a
        left join dim_license as b on a.license_id = b.license_id
        left join dim_product as c on a.product_id = c.product_id
        where a.date_id in 
               (20150630, 
                20150731, 
                20150831, 
                20150930, 
                20151031, 
                20151130, 
                20151231, 
                20160131, 
                20160229, 
                20160331, 
                20160430, 
                20160531, 
                20160630)
        and b.level = 'Full'
        and c.base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and c.platform = 'Server'
        group by 1,2,3
        )      
                        select license_id, sen
                        from opening 
                        where date_id = 20151031
                        and license_id not in (select license_id from opening where date_id = 20151130)
                        
                        ;
drop table if exists playground_bizops.scr_actual_churn_DecFY16;
create table playground_bizops.scr_actual_churn_DecFY16 as

        with opening as 
        (
        select date_id, a.license_id, b.sen
        from fact_license_active as a
        left join dim_license as b on a.license_id = b.license_id
        left join dim_product as c on a.product_id = c.product_id
        where a.date_id in 
               (20150630, 
                20150731, 
                20150831, 
                20150930, 
                20151031, 
                20151130, 
                20151231, 
                20160131, 
                20160229, 
                20160331, 
                20160430, 
                20160531, 
                20160630)
        and b.level = 'Full'
        and c.base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and c.platform = 'Server'
        group by 1,2,3
        )      
                        select license_id, sen
                        from opening 
                        where date_id = 20151130
                        and license_id not in (select license_id from opening where date_id = 20151231)
                ;
                 
drop table if exists playground_bizops.scr_actual_churn_JanFY16;
create table playground_bizops.scr_actual_churn_JanFY16 as

        with opening as 
        (
        select date_id, a.license_id, b.sen
        from fact_license_active as a
        left join dim_license as b on a.license_id = b.license_id
        left join dim_product as c on a.product_id = c.product_id
        where a.date_id in 
               (20150630, 
                20150731, 
                20150831, 
                20150930, 
                20151031, 
                20151130, 
                20151231, 
                20160131, 
                20160229, 
                20160331, 
                20160430, 
                20160531, 
                20160630)
        and b.level = 'Full'
        and c.base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and c.platform = 'Server'
        group by 1,2,3
        )      
                        select license_id, sen
                        from opening 
                        where date_id = 20151231
                        and license_id not in (select license_id from opening where date_id = 20160131)
               ; 
drop table if exists playground_bizops.scr_actual_churn_FebFY16;
create table playground_bizops.scr_actual_churn_FebFY16 as

        with opening as 
        (
        select date_id, a.license_id, b.sen
        from fact_license_active as a
        left join dim_license as b on a.license_id = b.license_id
        left join dim_product as c on a.product_id = c.product_id
        where a.date_id in 
               (20150630, 
                20150731, 
                20150831, 
                20150930, 
                20151031, 
                20151130, 
                20151231, 
                20160131, 
                20160229, 
                20160331, 
                20160430, 
                20160531, 
                20160630)
        and b.level = 'Full'
        and c.base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and c.platform = 'Server'
        group by 1,2,3
        )      
                        select license_id, sen
                        from opening 
                        where date_id = 20160131
                        and license_id not in (select license_id from opening where date_id = 20160229)
                ;
                
drop table if exists playground_bizops.scr_actual_churn_MarFY16;
create table playground_bizops.scr_actual_churn_MarFY16 as

        with opening as 
        (
        select date_id, a.license_id, b.sen
        from fact_license_active as a
        left join dim_license as b on a.license_id = b.license_id
        left join dim_product as c on a.product_id = c.product_id
        where a.date_id in 
               (20150630, 
                20150731, 
                20150831, 
                20150930, 
                20151031, 
                20151130, 
                20151231, 
                20160131, 
                20160229, 
                20160331, 
                20160430, 
                20160531, 
                20160630)
        and b.level = 'Full'
        and c.base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and c.platform = 'Server'
        group by 1,2,3
        )      
                        select license_id, sen
                        from opening 
                        where date_id = 20160229
                        and license_id not in (select license_id from opening where date_id = 20160331)
                
                
                ;
drop table if exists playground_bizops.scr_actual_churn_AprFY16;
create table playground_bizops.scr_actual_churn_AprFY16 as

        with opening as 
        (
        select date_id, a.license_id, b.sen
        from fact_license_active as a
        left join dim_license as b on a.license_id = b.license_id
        left join dim_product as c on a.product_id = c.product_id
        where a.date_id in 
               (20150630, 
                20150731, 
                20150831, 
                20150930, 
                20151031, 
                20151130, 
                20151231, 
                20160131, 
                20160229, 
                20160331, 
                20160430, 
                20160531, 
                20160630)
        and b.level = 'Full'
        and c.base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and c.platform = 'Server'
        group by 1,2,3
        )      
                        select license_id, sen
                        from opening 
                        where date_id = 20160331
                        and license_id not in (select license_id from opening where date_id = 20160430)
       ;
       
     drop table if exists playground_bizops.scr_actual_churn_MayFY16;
create table playground_bizops.scr_actual_churn_MayFY16 as

        with opening as 
        (
        select date_id, a.license_id, b.sen
        from fact_license_active as a
        left join dim_license as b on a.license_id = b.license_id
        left join dim_product as c on a.product_id = c.product_id
        where a.date_id in 
               (20150630, 
                20150731, 
                20150831, 
                20150930, 
                20151031, 
                20151130, 
                20151231, 
                20160131, 
                20160229, 
                20160331, 
                20160430, 
                20160531, 
                20160630)
        and b.level = 'Full'
        and c.base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and c.platform = 'Server'
        group by 1,2,3
        )      
                        select license_id, sen
                        from opening 
                        where date_id = 20160430
                        and license_id not in (select license_id from opening where date_id = 20160531)
             ; 
 drop table if exists playground_bizops.scr_actual_churn_JunFY16;
create table playground_bizops.scr_actual_churn_JunFY16 as

        with opening as 
        (
        select date_id, a.license_id, b.sen
        from fact_license_active as a
        left join dim_license as b on a.license_id = b.license_id
        left join dim_product as c on a.product_id = c.product_id
        where a.date_id in 
               (20150630, 
                20150731, 
                20150831, 
                20150930, 
                20151031, 
                20151130, 
                20151231, 
                20160131, 
                20160229, 
                20160331, 
                20160430, 
                20160531, 
                20160630)
        and b.level = 'Full'
        and c.base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and c.platform = 'Server'
        group by 1,2,3
        )      
                        select license_id, sen
                        from opening 
                        where date_id = 20160531
                        and license_id not in (select license_id from opening where date_id = 20160630)
               ; 
 drop table if exists playground_bizops.scr_actual_churn_JulFY17;
create table playground_bizops.scr_actual_churn_JulFY17 as

        with opening as 
        (
        select date_id, a.license_id, b.sen
        from fact_license_active as a
        left join dim_license as b on a.license_id = b.license_id
        left join dim_product as c on a.product_id = c.product_id
        where a.date_id in 
               (20150630, 
                20150731, 
                20150831, 
                20150930, 
                20151031, 
                20151130, 
                20151231, 
                20160131, 
                20160229, 
                20160331, 
                20160430, 
                20160531, 
                20160630)
        and b.level = 'Full'
        and c.base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and c.platform = 'Server'
        group by 1,2,3
        )      
                        select license_id, sen
                        from opening 
                        where date_id = 20160630
                        and license_id not in (select license_id from opening where date_id = 20160731)
                ;
        
  
