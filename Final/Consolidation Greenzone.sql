-- calculate sens where consolidation could have been the reason for churn

--create a table to store the results for later consolidation.
Drop Table if exists playground_bizops.scr_consolidation;
Create Table playground_bizops.scr_consolidation as

--create temporary table that contains the first date of each particular SEN.
with    
land_sen as 
        (--find the first date of the full license version of a SEN
        select  sen, 
                base_product,
                email_domain,
                min(financial_year) as financial_year,
                min(date) as min_date,
                min(user_limit) as user_limit
        from    public.sale
        where   license_level= 'Full'
        and     platform = 'Server'
        and     base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        group by 1,2,3
),
renewed_sens as 
        (--find the last renewal date of each sen found above, if they renewed
        select  a.sen,
                a.email_domain,
                max(b.date) as max_date
        from    land_sen as a 
        left join public.sale as b on a.sen = b.sen
        where   b.sale_type = 'Renewal'
        group by 1,2
        order by 1
),
upgraded_no_renew as 
        (--Find the sens that upgraded but have never renewed.
        select  a.sen,
                max(b.date) as max_date
        from    land_sen as a
        left join public.sale as b on a.sen = b.sen
        where   b.sale_type = 'Upgrade'
        and     b.date > a.min_date
        and     b.platform = 'Server'
        and     a.sen not in (select sen from renewed_sens)
        group by 1
        ),
combined_renewal as 
        (-- combine the results of renewed sens and upgraded-only sens
        select  sen,
                max_date
        from renewed_sens
        
        union
        
        select sen,
               max_date
        from upgraded_no_renew
        ),
non_renewed_churn_date as 
        (--this sets the expected churn date of 90 days after 1 year - only for those sens that havent renewed or upgraded.
        select  sen, 
                (min_date + 455) as churn_date
        from land_sen
        where sen not in (select sen from combined_renewal) 
),
renewed_churn_date as 
        (--this finds the expected churn date of 90 days after 1 year since last renewal or upgrade.
        select  sen, 
                (max_date + 455) as churn_date
        from    combined_renewal
)
,non_renewed_prod_count as 
        (--Counts how many of the same base product are owned +/- 90 days either side of 1 year since landing.      
        select  a.sen,
                a.email_domain,
                b.base_product,
                count(distinct b.sen) as prod_count
        from    land_sen as a
        left join public.license as b on a.email_domain = b.tech_email_domain
        left join non_renewed_churn_date as c on a.sen = c.sen
        where   a.sen in (select sen from non_renewed_churn_date)
        and     cast(b.start_date as date) between a.min_date + 270 and c.churn_date
        and     a.base_product = b.base_product
        and     b.platform = 'Server'
        group by 1,2,3
)      
,renewed_prod_count as 
        (--Counts how many of the same base product are owned +/- 90 days either side of 1 year since renewing/upgrading.       
        select  a.sen,
                a.email_domain,
                b.base_product,
                count(distinct b.sen) as prod_count
        from    land_sen as a
        left join public.license as b on a.email_domain = b.tech_email_domain
        left join renewed_churn_date as c on a.sen = c.sen
        where   a.sen in (select sen from renewed_churn_date)
        and cast(b.start_date as date) between a.min_date + 270 and c.churn_date
        and     a.base_product = b.base_product
        and     b.platform = 'Server'
        and     b.user_limit >= a.user_limit
        group by 1,2,3      
)
--brings together the sens of non-renewed and renewed/upgraded.
select  distinct a.sen,
        min(a.financial_year) as land_year         
        from land_sen as a
        where sen in (select sen from non_renewed_prod_count where prod_count > 1)
        group by 1
        
union 

select  distinct a.sen,
        min(a.financial_year) as land_year 
        from land_sen as a
        where sen in (select sen from renewed_prod_count where prod_count > 1)
        group by 1
;
select land_year, 
count(distinct sen) 
from playground_bizops.scr_consolidation 
group by 1
