drop table if exists playground_bizops.scr_migration;
create table playground_bizops.scr_migration as

--all product DC+Server migrations to Cloud
with migrations as (
select
        l.base_product,
        date_month(existing_end_date) as period,
        l.license_level,
        cast(t.sen as text) as server_sen
from license l
inner join
(
 select
        lr.tech_email,
        lr.tech_email_domain,
        lr.existing_user_limit,
        existing_end_date, 
        cast(l.sen as text), 
        lr.tech_region,
        lr.base_product
 from license_renewal lr
 inner join license l on (lr.sen = l.sen::text)
 where  1 = 1
        and l.base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        --and existing_end_date between date_trunc('month',current_date) - interval '80 months' and date_trunc('month',current_date) - interval '1 day'--'1 month 1 day'
        and lr.platform in('Server')
        and l.license_level in ('Full')
        and not renewed
) t
on (
        l.tech_email_domain = t.tech_email_domain
        --and l.tech_region = t.tech_region
        and l.base_product = t.base_product
        and l.purchase_date between existing_end_date - 90 and existing_end_date + 90
        and l.user_limit >= t.existing_user_limit)
  
where   1 = 1
        and l.base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and l.platform in('Server')
        and l.license_level in ('Full')
        --and l.purchase_date > date_trunc('month',current_date) - interval '80 months'
--group by 1,2
--order by 1,2
)
-- for table
select  a.server_sen as sen,
        min(a.period) as period,
        min(b.financial_year) as land_year
from migrations as a
left join sale as b on cast(a.server_sen as text) = b.sen
group by 1

;
select * from playground_bizops.scr_migration
;