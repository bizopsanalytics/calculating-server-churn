-- calculate sens where cross-grading could have been the reason for churn
Drop Table if exists playground_bizops.scr_cross_grade_prep;
create table playground_bizops.scr_cross_grade_prep as
with    
land_sen as (
        select  sen, 
                base_product,
                email_domain,
                quarter,
                min(financial_year) as financial_year,
                min(date) as min_date
        from    public.sale
        where   license_level= 'Full'
        and     platform = 'Server'
        and     base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        group by 1,2,3,4
),
renewed_sens as (
        select  a.sen,
                a.email_domain,
                b.quarter,
                max(b.date) as max_date
        from    land_sen as a 
        left join public.sale as b on a.sen = b.sen
        where   b.sale_type = 'Renewal'
        and     b.quarter > a.quarter
        group by 1,2,3
        order by 1
),
upgraded_no_renew as (
        select  a.sen,
                max(b.date) as max_date
        from    land_sen as a
        left join public.sale as b on a.sen = b.sen
        where   b.sale_type = 'Upgrade'
        and     b.date > a.min_date
        and     b.platform = 'Server'
        and     a.sen not in (select sen from renewed_sens)
        group by 1
        ),
combined_renewal as 
        (-- combine the results of renewed sens and upgraded-only sens
        select  sen,
                max_date
        from renewed_sens
        
        union
        
        select sen,
               max_date
        from upgraded_no_renew
        ),
land_churn_date as (
        select  sen, 
                (min_date + 365) as churn_date
        from land_sen
        where sen not in (select sen from combined_renewal) 
),
renewed_churn_date as (
        select  sen, 
                (max_date + 365) as churn_date
        from    combined_renewal
)
,non_renewed_prod_count as (      
        select  a.sen,
                a.email_domain,
                b.base_product,
                count(distinct b.sen) as prod_count
        from    land_sen as a
        left join public.license as b on a.email_domain = b.tech_email_domain
        left join land_churn_date as c on a.sen = c.sen
        where   a.sen in (select sen from land_churn_date)
        and     cast(b.start_date as date) between c.churn_date - 90 and c.churn_date + 90
        and     b.platform = 'Server'
        and     b.license_level = 'Full'
        group by 1,2,3
)      
,renewed_prod_count as (      
        select  a.sen,
                a.email_domain,
                b.base_product,
                count(distinct b.sen) as prod_count
        from    land_sen as a
        left join public.license as b on a.email_domain = b.tech_email_domain
        left join renewed_churn_date as c on a.sen = c.sen
        where   a.sen in (select sen from renewed_churn_date)
        and     cast(b.start_date as date) between c.churn_date - interval '90' DAY and c.churn_date + interval '90' DAY
        and     b.platform = 'Server'
        and     b.license_level = 'Full'
        group by 1,2,3      
),
final_multi_sen_list as (
select  a.financial_year, 
        a.sen,
        a.base_product as land_product,
        b.base_product as cross_product
        from land_sen as a
        left join non_renewed_prod_count as b on a.sen = b.sen
   
union 

select  a.financial_year, 
        a.sen,
        a.base_product as land_product,
        b.base_product as cross_product
        from land_sen as a
        left join renewed_prod_count as b on a.sen = b.sen
)
select  a.financial_year,
        a.land_product,
        a.cross_product, 
        b.max_date, 
        a.sen
from final_multi_sen_list as a
left join combined_renewal as b on a.sen=b.sen
where a.land_product in ('JIRA','JIRA Core', 'JIRA Software', 'FishEye', 'Crucible')
and a.cross_product in ('Bitbucket', 'JIRA Software', 'JIRA Core', 'JIRA Service Desk')
group by 1,2,3,4,5
order by 1,2,3,4
;
Drop Table if exists playground_bizops.scr_cross_grade;
create table playground_bizops.scr_cross_grade as

select  
        case 
        when land_product in ('FishEye', 'Crucible') and cross_product in ('Bitbucket') then 'fecrubitbucket'
        when land_product in ('JIRA') and cross_product in ('JIRA Core', 'JIRA Service Desk') then 'jiratocoredesk'
        when land_product in ('JIRA Software') and cross_product in ('JIRA Core', 'JIRA Service Desk') then 'jswtocoredesk'
        when land_product in ('JIRA Core') and cross_product in ('JIRA Service Desk') then 'coretodesk'
        end as "cross_grade",
        sen, 
        min(financial_year) as land_year, 
        max(max_date) as lost_date

from playground_bizops.cross_grade_prep
group by 1,2

