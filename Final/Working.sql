--find active licenses as of today
select  count(distinct sen) 
from license 
where license_level = 'Full'
and expiry_date > current_date
and platform = 'Server'
and base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
;
with 
actual_churn as 
(
        select date_id, a.license_id
        from fact_license_active as a
        left join dim_license as b on a.license_id = b.license_id
        left join dim_product as c on a.product_id = c.product_id
        where a.date_id in 
               (20150630, 
                20150731, 
                20150831, 
                20150930, 
                20151031, 
                20151130, 
                20151231, 
                20160131, 
                20160229, 
                20160331, 
                20160430, 
                20160531, 
                20160630)
        and b.level = 'Full'
        and c.base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and c.platform = 'Server'
        group by 1,2
)      
        select  date_id,
                case when date_id = 20150630 then (
                        select  count(distinct license_id)
                        from opening 
                        where date_id = 20150630
                        and license_id not in (select license_id from opening where date_id = 20150731)
                )  
                when date_id = 20150731 then (   
                        select  count(distinct license_id)
                        from opening 
                        where date_id = 20150731
                        and license_id not in (select license_id from opening where date_id = 20150831)
                ) 
                when date_id = 20150831 then (   
                        select  count(distinct license_id)
                        from opening 
                        where date_id = 20150831
                        and license_id not in (select license_id from opening where date_id = 20150930)
                ) 
                 when date_id = 20150930 then (   
                        select  count(distinct license_id)
                        from opening 
                        where date_id = 20150930
                        and license_id not in (select license_id from opening where date_id = 20151031)
                ) 
                 when date_id = 20151031 then (   
                        select  count(distinct license_id)
                        from opening 
                        where date_id = 20151031
                        and license_id not in (select license_id from opening where date_id = 20151130)
                ) 
                when date_id = 20151130 then (   
                        select  count(distinct license_id)
                        from opening 
                        where date_id = 20151130
                        and license_id not in (select license_id from opening where date_id = 20151231)
                ) 
                when date_id = 20151231 then (   
                        select  count(distinct license_id)
                        from opening 
                        where date_id = 20151231
                        and license_id not in (select license_id from opening where date_id = 20160131)
                ) 
                when date_id = 20160131 then (   
                        select  count(distinct license_id)
                        from opening 
                        where date_id = 20160131
                        and license_id not in (select license_id from opening where date_id = 20160229)
                ) 
                when date_id = 20160229 then (   
                        select  count(distinct license_id)
                        from opening 
                        where date_id = 20160229
                        and license_id not in (select license_id from opening where date_id = 20160331)
                ) 
                when date_id = 20160331 then (   
                        select  count(distinct license_id)
                        from opening 
                        where date_id = 20160331
                        and license_id not in (select license_id from opening where date_id = 20160430)
                ) 
                when date_id = 20160430 then (   
                        select  count(distinct license_id)
                        from opening 
                        where date_id = 20160430
                        and license_id not in (select license_id from opening where date_id = 20160531)
                ) 
                when date_id = 20160531 then (   
                        select  count(distinct license_id)
                        from opening 
                        where date_id = 20160531
                        and license_id not in (select license_id from opening where date_id = 20160630)
                ) 
                when date_id = 20160630 then (   
                        select  count(distinct license_id)
                        from opening 
                        where date_id = 20160630
                        and license_id not in (select license_id from opening where date_id = 20160731)
                ) 
                end as "churn count"
        from opening
        group by 1
        order by 1
