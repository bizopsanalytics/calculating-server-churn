select  b.financial_year,
        case 
        when a.product ilike '%Collab%' or a.product ilike '%conf%' then 'CollaborationFamily'
        when a.product ilike '%jira%' then 'JIRAFamily'
        when a.product ilike '%dev%' or a.product ilike '%bamboo%' or a.product ilike '%crucible%' or a.product ilike '%fish%' or a.product ilike '%bitbu%'  then 'DevToolFamily'
        when a.product ilike '%service%' then 'ServiceDeskFamily'
        end as "product",
        a.stage_reason,
        count(distinct a.sen) as sen_count
from playground_intsys.renewals_lost as a
left join playground_bizops.scr_remaining as b on a.sen = b.sen
where a.sen in (select sen from playground_bizops.scr_remaining)
group by 1,2,3
order by 1,2,4 desc