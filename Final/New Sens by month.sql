drop table if exists playground_bizops.scr_new_licenses;
create table playground_bizops.scr_new_licenses as

select          case 
                when existing_start_date between '2015-07-01' and '2015-07-31' then '07_2015'
                when existing_start_date between '2015-08-01' and '2015-08-31' then '08_2015'
                when existing_start_date between '2015-09-01' and '2015-09-30' then '09_2015'
                when existing_start_date between '2015-10-01' and '2015-10-31' then '10_2015'
                when existing_start_date between '2015-11-01' and '2015-11-30' then '11_2015'
                when existing_start_date between '2015-12-01' and '2015-12-31' then '12_2015'
                when existing_start_date between '2016-01-01' and '2016-01-31' then '01_2016'
                when existing_start_date between '2016-02-01' and '2016-02-29' then '02_2016'
                when existing_start_date between '2016-03-01' and '2016-03-31' then '03_2016' 
                when existing_start_date between '2016-04-01' and '2016-04-30' then '04_2016'
                when existing_start_date between '2016-05-01' and '2016-05-31' then '05_2016'
                when existing_start_date between '2016-06-01' and '2016-06-30' then '06_2016'
                when existing_start_date between '2016-07-01' and '2016-07-31' then '07_2016'
                when existing_start_date between '2016-08-01' and '2016-08-31' then '08_2016'
                when existing_start_date between '2016-09-01' and '2016-09-30' then '09_2016'
                when existing_start_date between '2016-10-01' and '2016-10-31' then '10_2016'
                when existing_start_date between '2016-11-01' and '2016-11-30' then '11_2016'
                when existing_start_date between '2016-12-01' and '2016-12-31' then '12_2016' 
                end as "Month",
                sen
from license_renewal
where  existing_license_level = 'Full'
and existing_start_date > '2015-06-30'
and platform = 'Server'
and existing_type = 'New'
and base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
group by 1,2
;

select "Month", count(distinct sen)
from playground_bizops.scr_new_licenses
group by 1
