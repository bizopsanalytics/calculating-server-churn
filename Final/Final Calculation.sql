--new sen per month-------
with new_sens_created as
( -- identify the minimum date of all SENs for core products.
        select  sen,
                start_date as creation_date
        from    license
        where   license_level = 'Full'
        and     base_product in (
                'JIRA',
                'JIRA Core',
                'JIRA Software',
                'JIRA Service Desk',
                'Confluence',
                'HipChat',
                'Bitbucket',
                'Bamboo',
                'FishEye',
                'Crucible')
        and     platform = 'Server'
        and     start_date > '2014-01-01'
        group by 1
)
 -- find out how many new SENs are created each month.
        select  case 
                when creation_date between '2015-07-01' and '2015-07-31' then '07_2015'
                when creation_date between '2015-08-01' and '2015-08-31' then '08_2015'
                when creation_date between '2015-09-01' and '2015-09-30' then '09_2015'
                when creation_date between '2015-10-01' and '2015-10-31' then '10_2015'
                when creation_date between '2015-11-01' and '2015-11-30' then '11_2015'
                when creation_date between '2015-12-01' and '2015-12-31' then '12_2015'
                when creation_date between '2016-01-01' and '2016-01-31' then '01_2016'
                when creation_date between '2016-02-01' and '2016-02-29' then '02_2016'
                when creation_date between '2016-03-01' and '2016-03-31' then '03_2016' 
                when creation_date between '2016-04-01' and '2016-04-30' then '04_2016'
                when creation_date between '2016-05-01' and '2016-05-31' then '05_2016'
                when creation_date between '2016-06-01' and '2016-06-30' then '06_2016'
                when creation_date between '2016-07-01' and '2016-07-31' then '07_2016'
                when creation_date between '2016-08-01' and '2016-08-31' then '08_2016'
                when creation_date between '2016-09-01' and '2016-09-30' then '09_2016'
                when creation_date between '2016-10-01' and '2016-10-31' then '10_2016'
                when creation_date between '2016-11-01' and '2016-11-30' then '11_2016'
                when creation_date between '2016-12-01' and '2016-12-31' then '12_2016' 
              end as "Month",
              count(distinct sen) as new_sens
              from new_sens_created
              where creation_date >= '2015-07-01'
              group by 1
              order by 1 asc

; 
--Opening Active Licenses --------
--snapshot of the number of active licenses at the end of each month
        select  date_id, 
                count(distinct a.license_id) as active_license_count
        from    fact_license_active as a
        left join dim_license as b on a.license_id = b.license_id
        left join dim_product as c on a.product_id = c.product_id
        where a.date_id in (
                20150630, 
                20150731, 
                20150831, 
                20150930, 
                20151031, 
                20151130, 
                20151231, 
                20160131, 
                20160229, 
                20160331, 
                20160430,
                20160531,
                20160630,
                20160731,
                20160831,
                20160930,
                20161031,
                20161130,
                20161231
                )
        and b.level = 'Full'
        and c.base_product in 
                (
                'JIRA',
                'JIRA Core',
                'JIRA Software',
                'JIRA Service Desk',
                'Confluence',
                'HipChat',
                'Bitbucket',
                'Bamboo',
                'FishEye',
                'Crucible'
                )
        and c.platform = 'Server'
        group by 1

;
--Total license churn --------
        with opening as 
        (
        select  date_id, 
                a.license_id
        from fact_license_active as a
        left join dim_license as b on a.license_id = b.license_id
        left join dim_product as c on a.product_id = c.product_id
        where a.date_id in 
               (20150630, 
                20150731, 
                20150831, 
                20150930, 
                20151031, 
                20151130, 
                20151231, 
                20160131, 
                20160229, 
                20160331, 
                20160430, 
                20160531, 
                20160630)
        and b.level = 'Full'
        and c.base_product in 
                ('JIRA',
                'JIRA Core',
                'JIRA Software',
                'JIRA Service Desk',
                'Confluence',
                'HipChat',
                'Bitbucket',
                'Bamboo',
                'FishEye',
                'Crucible'
                )
        and c.platform = 'Server'
        group by 1,2
        )      
        select  date_id,
                case when date_id = 20150731 then (
                        select  count(distinct license_id)
                        from opening 
                        where date_id = 20150630
                        and license_id not in (select license_id from opening where date_id = 20150731)
                )  
                when date_id = 20150831 then (   
                        select  count(distinct license_id)
                        from opening 
                        where date_id = 20150731
                        and license_id not in (select license_id from opening where date_id = 20150831)
                ) 
                when date_id = 20150930 then (   
                        select  count(distinct license_id)
                        from opening 
                        where date_id = 20150831
                        and license_id not in (select license_id from opening where date_id = 20150930)
                ) 
                 when date_id = 20151031 then (   
                        select  count(distinct license_id)
                        from opening 
                        where date_id = 20150930
                        and license_id not in (select license_id from opening where date_id = 20151031)
                ) 
                 when date_id = 20151130 then (   
                        select  count(distinct license_id)
                        from opening 
                        where date_id = 20151031
                        and license_id not in (select license_id from opening where date_id = 20151130)
                ) 
                when date_id = 20151231 then (   
                        select  count(distinct license_id)
                        from opening 
                        where date_id = 20151130
                        and license_id not in (select license_id from opening where date_id = 20151231)
                ) 
                when date_id = 20160131 then (   
                        select  count(distinct license_id)
                        from opening 
                        where date_id = 20151231
                        and license_id not in (select license_id from opening where date_id = 20160131)
                ) 
                when date_id = 20160229 then (   
                        select  count(distinct license_id)
                        from opening 
                        where date_id = 20160131
                        and license_id not in (select license_id from opening where date_id = 20160229)
                ) 
                when date_id = 20160331 then (   
                        select  count(distinct license_id)
                        from opening 
                        where date_id = 20160229
                        and license_id not in (select license_id from opening where date_id = 20160331)
                ) 
                when date_id = 20160430 then (   
                        select  count(distinct license_id)
                        from opening 
                        where date_id = 20160331
                        and license_id not in (select license_id from opening where date_id = 20160430)
                ) 
                when date_id = 20160531 then (   
                        select  count(distinct license_id)
                        from opening 
                        where date_id = 20160430
                        and license_id not in (select license_id from opening where date_id = 20160531)
                ) 
                when date_id = 20160630 then (   
                        select  count(distinct license_id)
                        from opening 
                        where date_id = 20160531
                        and license_id not in (select license_id from opening where date_id = 20160630)
                ) 
                when date_id = 20160731 then (   
                        select  count(distinct license_id)
                        from opening 
                        where date_id = 20160630
                        and license_id not in (select license_id from opening where date_id = 20160731)
                ) 
                end as "churn count"
        from opening
        group by 1
        order by 1
;
--Downgrades------        
with downgrades as 
(
        select  a.sen,
                max(b.expiry_date) as lost_date
        from    playground_bizops.scr_downgrade as a
        left join license as b on a.sen = cast(b.sen as text)
        where   b.expiry_date <= current_date
        and     b.expiry_date > '2010-06-30'
        group by 1
)          
                select
                case
                when lost_date between '2010-07-01' and '2011-06-30' then 'FY2011'
                when lost_date between '2011-07-01' and '2012-06-30' then 'FY2012'
                when lost_date between '2012-07-01' and '2013-06-30' then 'FY2013'
                when lost_date between '2013-07-01' and '2014-06-30' then 'FY2014' 
                when lost_date between '2014-07-01' and '2015-06-30' then 'FY2015'
                when lost_date between '2015-07-01' and '2016-06-30' then 'FY2016'
                when lost_date between '2016-07-01' and '2017-06-30' then 'FY2017'
                when lost_date between '2017-07-01' and '2018-06-30' then 'FY2018'
                end as lost_year,
                case
                when lost_date between '2015-07-01' and '2015-07-31' then 'JulFY2016'
                when lost_date between '2015-08-01' and '2015-08-31' then 'AugFY2016'
                when lost_date between '2015-09-01' and '2015-09-30' then 'SepFY2016'
                when lost_date between '2015-10-01' and '2015-10-31' then 'OctFY2016'
                when lost_date between '2015-11-01' and '2015-11-30' then 'NovFY2016'
                when lost_date between '2015-12-01' and '2015-12-31' then 'DecFY2016'
                when lost_date between '2016-01-01' and '2016-01-31' then 'JanFY2016'
                when lost_date between '2016-02-01' and '2016-02-28' then 'FebFY2016'
                when lost_date between '2016-03-01' and '2016-03-31' then 'MarFY2016' 
                when lost_date between '2016-04-01' and '2016-04-30' then 'AprFY2016'
                when lost_date between '2016-05-01' and '2016-05-31' then 'MayFY2016'
                when lost_date between '2016-06-01' and '2016-06-30' then 'JunFY2016'
                when lost_date between '2016-07-01' and '2016-07-31' then 'JulFY2017'
                when lost_date between '2016-08-01' and '2016-08-31' then 'AugFY2017'
                when lost_date between '2016-09-01' and '2016-09-30' then 'SepFY2017'
                when lost_date between '2016-10-01' and '2016-10-31' then 'OctFY2017'
                when lost_date between '2016-11-01' and '2016-11-30' then 'NovFY2017'
                when lost_date between '2016-12-01' and '2016-12-31' then 'DecFY2017'
                when lost_date between '2017-01-01' and '2017-01-30' then 'JanFY2017'
                when lost_date between '2017-02-01' and '2017-02-28' then 'FebFY2017'
                when lost_date between '2017-03-01' and '2017-03-31' then 'MarFY2017' 
                when lost_date between '2017-04-01' and '2017-04-30' then 'AprFY2017'
                when lost_date between '2017-05-01' and '2017-05-31' then 'MayFY2017'
                when lost_date between '2017-06-01' and '2017-06-30' then 'JunFY2017' 
                end as lost_month,
                sen
                from downgrades
                ;
--consolidation                
consolidation as 
(
        select  a.sen,
                max(b.expiry_date) as lost_date
        from playground_bizops.scr_consolidation as a
        left join license as b on a.sen = cast(b.sen as text)
        where   b.expiry_date <= current_date + 90
        and     b.expiry_date > '2010-06-30'
        and     a.sen not in (select sen from downgrades)
        group by 1
),
consolidation_time as
(               select
                case
                when lost_date between '2010-07-01' and '2011-06-30' then 'FY2011'
                when lost_date between '2011-07-01' and '2012-06-30' then 'FY2012'
                when lost_date between '2012-07-01' and '2013-06-30' then 'FY2013'
                when lost_date between '2013-07-01' and '2014-06-30' then 'FY2014' 
                when lost_date between '2014-07-01' and '2015-06-30' then 'FY2015'
                when lost_date between '2015-07-01' and '2016-06-30' then 'FY2016'
                when lost_date between '2016-07-01' and '2017-06-30' then 'FY2017'
                when lost_date between '2017-07-01' and '2018-06-30' then 'FY2018'
                end as lost_year,
                sen
                from consolidation
), 
migration as
(
        select  sen
        from    playground_bizops.scr_migration
        where   sen not in (select sen from downgrades)
        and     sen not in (select sen from consolidation)
),
crossgrade as 
(
        select  sen
        from    playground_bizops.scr_crossgrade
        where   sen not in (select sen from downgrades)
        and     sen not in (select sen from consolidation)
        and     sen not in (select sen from migration)
),
remaining as
(
        select sen
        from   playground_bizops.scr_remaining 
        where   sen not in (select sen from downgrades)
        and     sen not in (select sen from consolidation)
        and     sen not in (select sen from migration)
        and     sen not in (select sen from crossgrade)
),       
LA_grouped_reasons as
(
        select  
        b.financial_year as land_year,
        case 
        when a.product ilike '%Collab%' or a.product ilike '%conf%' then 'CollaborationFamily'
        when a.product ilike '%jira%' then 'JIRAFamily'
        when a.product ilike '%dev%' or a.product ilike '%bamboo%' or a.product ilike '%crucible%' or a.product ilike '%fish%' or a.product ilike '%bitbu%'  then 'DevToolFamily'
        when a.product ilike '%service%' then 'ServiceDeskFamily'
        end as "product",
        a.stage_reason,
        count(distinct a.sen) as sen_count
        from playground_intsys.renewals_lost as a
        left join playground_bizops.scr_remaining as b on a.sen = b.sen
        where a.sen in (select sen from remaining)
        group by 1,2,3
        order by 1,2,3 desc
),
la_reason_description as
(
        select 
        case
                when lost_date between '2002-07-01' and '2010-06-30' then 'Pre_FY2011'
                when lost_date between '2010-07-01' and '2011-06-30' then 'FY2011'
                when lost_date between '2011-07-01' and '2012-06-30' then 'FY2012'
                when lost_date between '2012-07-01' and '2013-06-30' then 'FY2013'
                when lost_date between '2013-07-01' and '2014-06-30' then 'FY2014' 
                when lost_date between '2014-07-01' and '2015-06-30' then 'FY2015'
                when lost_date between '2015-07-01' and '2016-06-30' then 'FY2016'
                when lost_date between '2016-07-01' and '2017-06-30' then 'FY2017'
        end as lost_year,
                case
                when lost_date between '2016-07-01' and '2016-07-31' then 'JulFY2017'
                when lost_date between '2016-08-01' and '2016-08-31' then 'AugFY2017'
                when lost_date between '2016-09-01' and '2016-09-30' then 'SepFY2017'
                when lost_date between '2016-10-01' and '2016-10-31' then 'OctFY2017'
                when lost_date between '2016-11-01' and '2016-11-30' then 'NovFY2017'
                when lost_date between '2016-12-01' and '2016-12-31' then 'DecFY2017'
                when lost_date between '2017-01-01' and '2017-01-31' then 'JanFY2017'
                when lost_date between '2017-02-01' and '2017-02-28' then 'FebFY2017'
                when lost_date between '2017-03-01' and '2017-03-31' then 'MarFY2017' 
                when lost_date between '2017-04-01' and '2017-04-30' then 'AprFY2017'
                when lost_date between '2017-05-01' and '2017-05-31' then 'MayFY2017'
                when lost_date between '2017-06-01' and '2017-06-30' then 'JunFY2017' 
        end as lost_month_2017, 
        case 
        when a.product ilike '%Collab%' or a.product ilike '%conf%' then 'CollaborationFamily'
        when a.product ilike '%jira%' then 'JIRAFamily'
        when a.product ilike '%dev%' or a.product ilike '%bamboo%' or a.product ilike '%crucible%' or a.product ilike '%fish%' or a.product ilike '%bitbu%'  then 'DevToolFamily'
        when a.product ilike '%service%' then 'ServiceDeskFamily'
        end as "Product",
        a.stage_reason,
        a.description,
        a.sen
        from playground_intsys.renewals_lost as a
        left join playground_bizops.scr_remaining as b on a.sen = b.sen
        where a.sen in (select sen from playground_bizops.scr_remaining)
        and a.description is not null
        and a.description not ilike '%salesforce%'
        and a.description not ilike '%see%'
        and a.description not ilike '%support.atlassian%'
        and a.description not ilike '%+4%'
        and a.description not ilike '%www.%'
        and a.description not ilike '%delet%'
        group by 1,2,3,4,5,6
        order by 1,2,3,4 desc
),
la_reasons_time as 
(       select         
        case
                when lost_date between '2002-07-01' and '2010-06-30' then 'Pre_FY2011'
                when lost_date between '2010-07-01' and '2011-06-30' then 'FY2011'
                when lost_date between '2011-07-01' and '2012-06-30' then 'FY2012'
                when lost_date between '2012-07-01' and '2013-06-30' then 'FY2013'
                when lost_date between '2013-07-01' and '2014-06-30' then 'FY2014' 
                when lost_date between '2014-07-01' and '2015-06-30' then 'FY2015'
                when lost_date between '2015-07-01' and '2016-06-30' then 'FY2016'
                when lost_date between '2016-07-01' and '2017-06-30' then 'FY2017'
        end as lost_year,
                case
                when lost_date between '2016-07-01' and '2016-07-31' then 'JulFY2017'
                when lost_date between '2016-08-01' and '2016-08-31' then 'AugFY2017'
                when lost_date between '2016-09-01' and '2016-09-30' then 'SepFY2017'
                when lost_date between '2016-10-01' and '2016-10-31' then 'OctFY2017'
                when lost_date between '2016-11-01' and '2016-11-30' then 'NovFY2017'
                when lost_date between '2016-12-01' and '2016-12-31' then 'DecFY2017'
                when lost_date between '2017-01-01' and '2017-01-31' then 'JanFY2017'
                when lost_date between '2017-02-01' and '2017-02-28' then 'FebFY2017'
                when lost_date between '2017-03-01' and '2017-03-31' then 'MarFY2017' 
                when lost_date between '2017-04-01' and '2017-04-30' then 'AprFY2017'
                when lost_date between '2017-05-01' and '2017-05-31' then 'MayFY2017'
                when lost_date between '2017-06-01' and '2017-06-30' then 'JunFY2017' 
        end as lost_month_2017,
        case 
                when product ilike '%Collab%' or product ilike '%conf%' then 'CollaborationFamily'
                when product ilike '%jira%' then 'JIRAFamily'
                when product ilike '%dev%' or product ilike '%bamboo%' or product ilike '%crucible%' or product ilike '%fish%' or product ilike '%bitbu%'  then 'DevToolFamily'
                when product ilike '%service%' then 'ServiceDeskFamily'
                else 'Other'
        end as "Product",
        stage_reason,  
        count(distinct sen) as sen_count
        from playground_intsys.renewals_lost 
        group by 1,2,3,4
        order by 1,2,4 desc
)
,
LA_regrettable_class as
(
        select  a.sen,
                b.stage_reason,
                case
                when b.stage_reason in 
                        (
                        'Company Out of Business',
                        'Consolidating Instances',
                        'Using another instance (same product)',
                        'Cross-graded to JIRA Core',
                        'Invalid Opportunity',
                        'Moved to Cloud',
                        'Moved to Data Center',
                        'Moved to another BTF product',
                        'Project Completed',
                        'Waiting to Upgrade User-Tiers'
                        )
                then 'Non-Regrettable'
                when b.stage_reason in 
                        (
                        'Using Competitors Product',
                        'Downgrade to Starter',
                        'No longer use Product',
                        'No need for Support / Updates',
                        'Not satisfied with Support / Updates',
                        'JIRA Renaissance Package / Pricing',
                        'Too expensive / No budget',
                        'Waiting to Update Product',
                        'Invalid Contact Information',
                        'No Response',
                        'Other (see General Comments)',
                        'Other/no additional information given',
                        'Reason not given',
                        'Unable to Contact'
                        )
                then 'Regrettable'
                --else 'Unknown'
                end as "classi"
        from remaining as a
        left join playground_intsys.renewals_lost as b on a.sen = b.sen
)        


--select count(distinct sen) from playground_bizops.scr_actual_churn_junFY16 where sen in (select sen from consolidation) or sen in (select sen from migration) or sen in (select sen from crossgrade)
--select count(distinct sen) from playground_bizops.scr_actual_churn_junFY16 where sen in (select sen from downgrades where lost_date between '2016-06-01' and '2016-06-30')       
--select classi,  count(distinct sen) from LA_regrettable_class where sen in (select sen from playground_bizops.scr_actual_churn_junFY16) and sen not in (select sen from downgrades) and sen not in (select sen from consolidation) and sen not in (select sen from migration) and sen not in (select sen from crossgrade) group by 1


--select * from la_reasons_time                                                 --Loyalty advocate grouped by year lost for all sens
--select * from la_reasons_time where sen in (select sen from remaining)        --Loyalty advocate grouped by year lost for the remaining group
--select * from la_reason_description                                           --Deep dive into LA feedback free text for the remaining group
--select * from la_grouped_reasons                                              --Summary of LA churn reasons for the remaining group

--select lost_year, lost_month, count(distinct sen) from downgrades_time group by 1,2 order by 1,2           --Downgrades over time

