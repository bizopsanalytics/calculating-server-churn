with classification as ( 
 select         sen,
                stage_reason,
                case
                when stage_reason in 
                        (
                        'Company Out of Business',
                        'Consolidating Instances',
                        'Using another instance (same product)',
                        'Cross-graded to JIRA Core',
                        'Invalid Opportunity',
                        'Moved to Cloud',
                        'Moved to Data Center',
                        'Moved to another BTF product',
                        'Project Completed',
                        'Waiting to Upgrade User-Tiers'
                        )
                then 'Non-Regrettable'
                when stage_reason in 
                        (
                        'Using Competitors Product',
                        'Downgrade to Starter',
                        'No longer use Product',
                        'No need for Support / Updates',
                        'Not satisfied with Support / Updates',
                        'JIRA Renaissance Package / Pricing',
                        'Too expensive / No budget',
                        'Waiting to Update Product',
                        'Invalid Contact Information',
                        'No Response',
                        'Other (see General Comments)',
                        'Other/no additional information given',
                        'Reason not given',
                        'Unable to Contact'
                        )
                then 'Regrettable'
                else 'Unknown'
                end as "classi",
                case
                when stage_reason in 
                        (
                        'Company Out of Business',
                        'Cross-graded to JIRA Core',
                        'Invalid Opportunity', 
                        'Project Completed',
                        'Waiting to Upgrade User-Tiers',
                        'Using Competitors Product',
                        'Downgrade to Starter',
                        'Not satisfied with Support / Updates',
                        'JIRA Renaissance Package / Pricing'
                        )
                then 'Not_Migration'
                when stage_reason in 
                        (
                        'Consolidating Instances',
                        'Using another instance (same product)',
                        'Moved to Cloud',
                        'Moved to Data Center',
                        'Moved to another BTF product',
                        'No longer use Product',
                        'No need for Support / Updates',
                        'Too expensive / No budget',
                        'Waiting to Update Product',
                        'Invalid Contact Information',
                        'No Response',
                        'Other (see General Comments)',
                        'Other/no additional information given',
                        'Reason not given',
                        'Unable to Contact'
                        )
                then 'Possible_Migration'
                else 'Unknown'
                end as "migration_status"
        from  playground_intsys.renewals_lost
        )
       
        select  a.sen,
                a.period,
                b.stage_reason,
                b.migration_status
        from    playground_bizops.scr_migration as a
        left join classification as b on a.sen = b.sen
        group by 1,2,3