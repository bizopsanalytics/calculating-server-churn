--Opening Active Licenses --------
--snapshot of the number of active licenses at the end of each month
        select  date_id, 
                count(distinct a.license_id) as active_license_count
        from    fact_license_active as a
        left join dim_license as b on a.license_id = b.license_id
        left join dim_product as c on a.product_id = c.product_id
        where a.date_id in (
                20150630, 
                20150731, 
                20150831, 
                20150930, 
                20151031, 
                20151130, 
                20151231, 
                20160131, 
                20160229, 
                20160331, 
                20160430,
                20160531,
                20160630,
                20160731,
                20160831,
                20160930,
                20161031,
                20161130,
                20161231
                )
        and b.level = 'Full'
        and c.base_product in 
                (
                'JIRA',
                'JIRA Core',
                'JIRA Software',
                'JIRA Service Desk',
                'Confluence',
                'HipChat',
                'Bitbucket',
                'Bamboo',
                'FishEye',
                'Crucible'
                )
        and c.platform = 'Server'
        group by 1

;

--new licenses in period
drop table if exists playground_bizops.scr_new_licenses;
create table playground_bizops.scr_new_licenses as

select          case 
                when existing_start_date between '2015-07-01' and '2015-07-31' then '2015_07'
                when existing_start_date between '2015-08-01' and '2015-08-31' then '2015_08'
                when existing_start_date between '2015-09-01' and '2015-09-30' then '2015_09'
                when existing_start_date between '2015-10-01' and '2015-10-31' then '2015_10'
                when existing_start_date between '2015-11-01' and '2015-11-30' then '2015_11'
                when existing_start_date between '2015-12-01' and '2015-12-31' then '2015_12'
                when existing_start_date between '2016-01-01' and '2016-01-31' then '2016_01'
                when existing_start_date between '2016-02-01' and '2016-02-29' then '2016_02'
                when existing_start_date between '2016-03-01' and '2016-03-31' then '2016_03' 
                when existing_start_date between '2016-04-01' and '2016-04-30' then '2016_04'
                when existing_start_date between '2016-05-01' and '2016-05-31' then '2016_05'
                when existing_start_date between '2016-06-01' and '2016-06-30' then '2016_06'
                when existing_start_date between '2016-07-01' and '2016-07-31' then '2016_07'
                when existing_start_date between '2016-08-01' and '2016-08-31' then '2016_08'
                when existing_start_date between '2016-09-01' and '2016-09-30' then '2016_09'
                when existing_start_date between '2016-10-01' and '2016-10-31' then '2016_10'
                when existing_start_date between '2016-11-01' and '2016-11-30' then '2016_11'
                when existing_start_date between '2016-12-01' and '2016-12-31' then '2016_12' 
                end as period,
                sen
from license_renewal
where  existing_license_level = 'Full'
and existing_start_date > '2015-06-30'
and platform = 'Server'
and existing_type = 'New'
and base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
group by 1,2
;
--summary of new licenses in period
select period, count(distinct sen)
from playground_bizops.scr_new_licenses
group by 1
;
--expired licenses
drop table if exists playground_bizops.scr_expired_licenses;
create table playground_bizops.scr_expired_licenses as

select  case 
                when existing_end_date between '2015-07-01' and '2015-07-31' then '2015_07'
                when existing_end_date between '2015-08-01' and '2015-08-31' then '2015_08'
                when existing_end_date between '2015-09-01' and '2015-09-30' then '2015_09'
                when existing_end_date between '2015-10-01' and '2015-10-31' then '2015_10'
                when existing_end_date between '2015-11-01' and '2015-11-30' then '2015_11'
                when existing_end_date between '2015-12-01' and '2015-12-31' then '2015_12'
                when existing_end_date between '2016-01-01' and '2016-01-31' then '2016_01'
                when existing_end_date between '2016-02-01' and '2016-02-29' then '2016_02'
                when existing_end_date between '2016-03-01' and '2016-03-31' then '2016_03' 
                when existing_end_date between '2016-04-01' and '2016-04-30' then '2016_04'
                when existing_end_date between '2016-05-01' and '2016-05-31' then '2016_05'
                when existing_end_date between '2016-06-01' and '2016-06-30' then '2016_06'
                when existing_end_date between '2016-07-01' and '2016-07-31' then '2016_07'
                when existing_end_date between '2016-08-01' and '2016-08-31' then '2016_08'
                when existing_end_date between '2016-09-01' and '2016-09-30' then '2016_09'
                when existing_end_date between '2016-10-01' and '2016-10-31' then '2016_10'
                when existing_end_date between '2016-11-01' and '2016-11-30' then '2016_11'
                when existing_end_date between '2016-12-01' and '2016-12-31' then '2016_12' 
                end as period,
                existing_end_date,
                sen
from license_renewal 
where  existing_license_level = 'Full'
and existing_end_date > '2015-06-30'
and platform = 'Server'
and base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
group by 1,2,3;

-- for getting a count of real license churn (expired licenses)
with active as (
        select  a.period,
        a.sen
from playground_bizops.scr_expired_licenses as a
left join license as b on a.sen =  cast(b.sen as text)
where b.expiry_date >= current_date
group by 1,2
)
select  period, 
        count (distinct sen)
from playground_bizops.scr_expired_licenses
where sen not in (select sen from active)
group by 1
;
-- for creating the churned sens
drop table if exists playground_bizops.scr_churned_licenses;
create table playground_bizops.scr_churned_licenses as

with active as (
        select  a.period,
        a.sen
from playground_bizops.scr_expired_licenses as a
left join license as b on a.sen =  cast(b.sen as text)
where b.expiry_date >= current_date
group by 1,2
)
select period, sen
from playground_bizops.scr_expired_licenses
where sen not in (select sen from active)
;
 -- for identifying the definable regretted churn

select period, count(distinct sen)
from playground_bizops.scr_churned_licenses
where sen in (select sen from playground_bizops.scr_downgrades)
group by 1
;
--count the number of defined non-regrettable churn
select  period,
        count(distinct sen)
from playground_bizops.scr_churned_licenses
where --sen in (select sen from playground_bizops.scr_consolidation)
--or 
sen in (select sen from playground_bizops.scr_migration)
--or 
--sen in (select sen from playground_bizops.scr_crossgrade)
group by 1
;

-- Classify the LA churned sens
with classification as ( 
 select         a.sen,
                a.period,
                b.stage_reason,
                case
                when b.stage_reason in 
                        (
                        'Company Out of Business',
                        'Consolidating Instances',
                        'Using another instance (same product)',
                        'Cross-graded to JIRA Core',
                        'Invalid Opportunity',
                        'Moved to Cloud',
                        'Moved to Data Center',
                        'Moved to another BTF product',
                        'Project Completed',
                        'Waiting to Upgrade User-Tiers'
                        )
                then 'Non-Regrettable'
                when b.stage_reason in 
                        (
                        'Using Competitors Product',
                        'Downgrade to Starter',
                        'No longer use Product',
                        'No need for Support / Updates',
                        'Not satisfied with Support / Updates',
                        'JIRA Renaissance Package / Pricing',
                        'Too expensive / No budget',
                        'Waiting to Update Product',
                        'Invalid Contact Information',
                        'No Response',
                        'Other (see General Comments)',
                        'Other/no additional information given',
                        'Reason not given',
                        'Unable to Contact'
                        )
                then 'Regrettable'
                else 'Unknown'
                end as "classi"
        from playground_bizops.scr_churned_licenses as a
        left join playground_intsys.renewals_lost as b on a.sen = b.sen
        )
        
        select  period, 
                classi, 
                count(distinct sen)
        from classification
        group by 1,2
        order by 1,2
        

; 
--overlay the LA data with the regrettable churn (all regretted churn)
with nonregret as 
(
select  sen
from playground_bizops.scr_churned_licenses
where sen in (select sen from playground_bizops.scr_consolidation)
or sen in (select sen from playground_bizops.scr_migration)
or sen in (select sen from playground_bizops.scr_crossgrade)
group by 1
),
regret as (
select  period,
        sen
from playground_bizops.scr_churned_licenses
where sen not in (select sen from nonregret)
group by 1,2
),
classification as ( 
 select         a.sen,
                a.period,
                b.stage_reason,
                case
                when b.stage_reason in 
                        (
                        'Company Out of Business',
                        'Consolidating Instances',
                        'Using another instance (same product)',
                        'Cross-graded to JIRA Core',
                        'Invalid Opportunity',
                        'Moved to Cloud',
                        'Moved to Data Center',
                        'Moved to another BTF product',
                        'Project Completed',
                        'Waiting to Upgrade User-Tiers'
                        )
                then 'Non-Regrettable'
                when b.stage_reason in 
                        (
                        'Using Competitors Product',
                        'Downgrade to Starter',
                        'No longer use Product',
                        'No need for Support / Updates',
                        'Not satisfied with Support / Updates',
                        'JIRA Renaissance Package / Pricing',
                        'Too expensive / No budget',
                        'Waiting to Update Product',
                        'Invalid Contact Information',
                        'No Response',
                        'Other (see General Comments)',
                        'Other/no additional information given',
                        'Reason not given',
                        'Unable to Contact'
                        )
                then 'Regrettable'
                else 'Unknown'
                end as "classi"
        from regret as a
        left join playground_intsys.renewals_lost as b on a.sen = b.sen
        )
        
        select period, classi, count(distinct sen)
        from classification
        group by 1,2
        order by 1,2
        ;

--overlay the LA data with the regrettable churn (only regretted churn)
with nonregret as 
(
select  sen
from playground_bizops.scr_churned_licenses
where sen in (select sen from playground_bizops.scr_consolidation)
or sen in (select sen from playground_bizops.scr_migration)
or sen in (select sen from playground_bizops.scr_crossgrade)
group by 1
),
regret as (
select  period,
        sen
from playground_bizops.scr_churned_licenses
where sen in (select sen from playground_bizops.scr_downgrades)
group by 1,2
),
classification as ( 
 select         a.sen,
                a.period,
                b.stage_reason,
                case
                when b.stage_reason in 
                        (
                        'Company Out of Business',
                        'Consolidating Instances',
                        'Using another instance (same product)',
                        'Cross-graded to JIRA Core',
                        'Invalid Opportunity',
                        'Moved to Cloud',
                        'Moved to Data Center',
                        'Moved to another BTF product',
                        'Project Completed',
                        'Waiting to Upgrade User-Tiers'
                        )
                then 'Non-Regrettable'
                when b.stage_reason in 
                        (
                        'Using Competitors Product',
                        'Downgrade to Starter',
                        'No longer use Product',
                        'No need for Support / Updates',
                        'Not satisfied with Support / Updates',
                        'JIRA Renaissance Package / Pricing',
                        'Too expensive / No budget',
                        'Waiting to Update Product',
                        'Invalid Contact Information',
                        'No Response',
                        'Other (see General Comments)',
                        'Other/no additional information given',
                        'Reason not given',
                        'Unable to Contact'
                        )
                then 'Regrettable'
                else 'Unknown'
                end as "classi"
        from regret as a
        left join playground_intsys.renewals_lost as b on a.sen = b.sen
        )
        
        select period, classi, count(distinct sen)
        from classification
        group by 1,2
        order by 1,2
        ;        
--overlay the LA data with the nonregrettable churn
with nonregret as 
(
select  period, sen
from playground_bizops.scr_churned_licenses
where sen in (select sen from playground_bizops.scr_consolidation)
or sen in (select sen from playground_bizops.scr_migration)
or sen in (select sen from playground_bizops.scr_crossgrade)
group by 1,2
),
regret as (
select  period,
        sen
from playground_bizops.scr_churned_licenses
where sen not in (select sen from nonregret)
group by 1,2
),
classification as ( 
 select         a.sen,
                a.period,
                b.stage_reason,
                case
                when b.stage_reason in 
                        (
                        'Company Out of Business',
                        'Consolidating Instances',
                        'Using another instance (same product)',
                        'Cross-graded to JIRA Core',
                        'Invalid Opportunity',
                        'Moved to Cloud',
                        'Moved to Data Center',
                        'Moved to another BTF product',
                        'Project Completed',
                        'Waiting to Upgrade User-Tiers'
                        )
                then 'Non-Regrettable'
                when b.stage_reason in 
                        (
                        'Using Competitors Product',
                        'Downgrade to Starter',
                        'No longer use Product',
                        'No need for Support / Updates',
                        'Not satisfied with Support / Updates',
                        'JIRA Renaissance Package / Pricing',
                        'Too expensive / No budget',
                        'Waiting to Update Product',
                        'Invalid Contact Information',
                        'No Response',
                        'Other (see General Comments)',
                        'Other/no additional information given',
                        'Reason not given',
                        'Unable to Contact'
                        )
                then 'Regrettable'
                else 'Unknown'
                end as "classi"
        from nonregret as a
        left join playground_intsys.renewals_lost as b on a.sen = b.sen
        )
        
        select period, classi, count(distinct sen)
        from classification
        group by 1,2
        order by 1,2