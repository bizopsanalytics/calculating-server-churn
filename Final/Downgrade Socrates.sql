drop table if exists zone_bizops.scr_downgrades;
create table zone_bizops.scr_downgrades as

-- Calculates SENs that can be attributed to downgrades.

with    
land_sen as (
        select  sen, 
                base_product,
                email_domain,
                quarter,
                financial_year,
                min(date) as min_date
        from    public.sale
        where   license_level= 'Full'
        and     platform = 'Server'
        and     base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        group by 1,2,3,4,5
),
downgrade_samesen as (
        select  a.sen
        from    land_sen as a 
        left join public.sale as b on a.sen = b.sen
        where   b.platform = 'Server'
        and     b.license_level = 'Starter'
        and     b.date >= a.min_date
        group by 1
),
renewed_sens as (
        select  a.sen,
                a.email_domain,
                b.quarter,
                max(b.date) as max_date
        from    land_sen as a 
        left join public.sale as b on a.sen = b.sen
        where   b.sale_type = 'Renewal'
        and     b.quarter > a.quarter
        group by 1,2,3
        order by 1
),
upgraded_no_renew as (
        select  a.sen,
                b.quarter,
                max(b.date) as max_date
        from    land_sen as a
        left join public.sale as b on a.sen = b.sen
        where   b.sale_type = 'Upgrade'
        and     b.date > a.min_date
        and     b.platform = 'Server'
        and     a.sen not in (select sen from renewed_sens)
        group by 1,2
        ),
combined_renewal as 
        (-- combine the results of renewed sens and upgraded-only sens
        select  sen,
                quarter,
                max_date
        from renewed_sens
        
        union
        
        select  sen,
                quarter,
                max_date
        from upgraded_no_renew
        ),
starter_land as (
        select  sen, 
                email_domain,
                base_product,
                min(date) as min_start
        from public.sale
        where   sale_type = 'Starter'
        and     platform = 'Server'
        group by 1,2,3
),
renewed_starter as (
        select  a.sen as affected_sen,
                a.email_domain
        from    land_sen as a
        left join starter_land as b on a.email_domain = b.email_domain
        left join combined_renewal as c on a.sen = c.sen
        where   a.base_product = b.base_product
        and     b.min_start > c.max_date + interval '275' DAY
        and     b.min_start < c.max_date + interval '455' DAY
        and     a.quarter < c.quarter
        group by 1,2
        order by 1
),
non_renewed_starter as (
        select a.sen as affected_sen,
        a.email_domain
        from    land_sen as a
        left join starter_land as b on a.email_domain = b.email_domain
        where   a.base_product = b.base_product
        and     b.min_start > a.min_date + interval '275' DAY
        and     b.min_start < a.min_date + interval '455' DAY
        and     a.sen not in (select sen from combined_renewal)
        group by 1,2
        order by 1,2
),
active_prod as (
        select  b.tech_email_domain,
                b.base_product,
                count(distinct b.sen) as prod_count
        from    land_sen as a
        left join public.license as b on a.email_domain = b.tech_email_domain
        left join starter_land as c on a.email_domain = c.email_domain and a.base_product = c.base_product
        where   b.last_license_level = 'Full'
        and     cast(b.paid_license_end_date as date) < c.min_start
        and     cast(b.paid_license_end_date as date) > c.min_start - interval '90' DAY
        group by 1,2
),
final_sen_downgrade_new as (
        select  distinct a.affected_sen
        from    renewed_starter as a
        join    active_prod as c on a.email_domain = c.tech_email_domain  
        and     a.affected_sen not in (select sen from downgrade_samesen)
        and     c.prod_count = 1

union
        
        select  distinct b.affected_sen
        from    non_renewed_starter as b
        join    active_prod as c on b.email_domain = c.tech_email_domain  
        and     b.affected_sen not in (select sen from downgrade_samesen)
        and     c.prod_count = 1
        group by 1
        order by 1
)
        select  a.affected_sen as sen, min(b.financial_year) as land_year
        from    final_sen_downgrade_new as a
        left join land_sen as b on a.affected_sen=b.sen
        group by 1
        
        union
        
        select a.sen as sen, min(b.financial_year) as land_year
        from downgrade_samesen as a
        left join land_sen as b on a.sen = b.sen
        group by 1

