-- calculate sens where consolidation could have been the reason for churn

--create a table to store the results for later consolidation.
Drop Table if exists zone_bizops.scr_consolidation;
Create Table zone_bizops.scr_consolidation as

--create temporary table that contains the first date of each particular SEN.
with    
land_sen as (
        select  sen, 
                base_product,
                email_domain,
                quarter,
                financial_year,
                min(date) as min_date
        from    public.sale
        where   license_level= 'Full'
        and     platform = 'Server'
        and     base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and     financial_year > 'FY2010'
        and     financial_year <> 'FY2017'
        group by 1,2,3,4,5
),
renewed_sens as (
        select  a.sen,
                a.email_domain,
                b.quarter,
                max(b.date) as max_date
        from    land_sen as a 
        left join public.sale as b on a.sen = b.sen
        where   b.sale_type = 'Renewal'
        and     b.quarter > a.quarter
        group by 1,2,3
        order by 1
),
upgraded_no_renew as (
        select  a.sen,
                max(b.date) as max_date
        from    land_sen as a
        left join public.sale as b on a.sen = b.sen
        where   b.sale_type = 'Upgrade'
        and     b.date > a.min_date
        and     b.platform = 'Server'
        and     a.sen not in (select sen from renewed_sens)
        group by 1
        ),
combined_renewal as 
        (-- combine the results of renewed sens and upgraded-only sens
        select  sen,
                max_date
        from renewed_sens
        
        union
        
        select sen,
               max_date
        from upgraded_no_renew
        ),
land_churn_date as (
        select  sen, 
                (min_date + interval '545' day) as churn_date
        from land_sen
        where sen not in (select sen from combined_renewal) 
),
renewed_churn_date as (
        select  sen, 
                (max_date + interval '545' day) as churn_date
        from    combined_renewal
)
,non_renewed_prod_count as (      
        select  a.sen,
                a.email_domain,
                b.base_product,
                count(distinct b.sen) as prod_count
        from land_sen as a
        left join public.license as b on a.email_domain = b.tech_email_domain
        left join land_churn_date as c on a.sen = c.sen
        where a.sen in (select sen from land_churn_date)
        and cast(b.paid_license_start_date as date) between a.min_date and c.churn_date
        and a.base_product = b.base_product
        and b.platform = 'Server'
        group by 1,2,3
)      
,renewed_prod_count as (      
        select  a.sen,
                a.email_domain,
                b.base_product,
                count(distinct b.sen) as prod_count
        from land_sen as a
        left join public.license as b on a.email_domain = b.tech_email_domain
        left join renewed_churn_date as c on a.sen = c.sen
        where a.sen in (select sen from renewed_churn_date)
        and cast(b.paid_license_start_date as date) between a.min_date and c.churn_date
        and a.base_product = b.base_product
        and b.platform = 'Server'
        group by 1,2,3      
)
select  a.financial_year as land_year, 
        a.sen
        from land_sen as a
        left join non_renewed_prod_count as b on a.sen = b.sen
        where b.prod_count > 1
        
union 

select  a.financial_year as land_year, 
        a.sen
        from land_sen as a
        left join renewed_prod_count as b on a.sen = b.sen
        where b.prod_count > 1


