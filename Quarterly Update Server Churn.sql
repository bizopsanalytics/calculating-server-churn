--number of active Server customers/SENs at the start of the quarter
with opening_SEN_Count as (
        select  *
        from public.license
        where platform = 'Server'
        and first_paid_license_level = 'Full'
        and paid_license_end_date > '2016-03-31'
        and base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
)
select  count(distinct tech_email_domain) as opening_cust_count,
        count(distinct sen) as opening_sen_count
from    opening_SEN_Count;

-- number of new+returning Server customer SENs added in the period
with land_date as (
        select  *
        from public.license
        where platform = 'Server'
        and first_paid_license_level = 'Full'
        and paid_license_start_date < '2016-07-01'
        and paid_license_start_date > '2016-03-31'
        and base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
)
select  count(distinct tech_email_domain) as new_cust_count,
        count(distinct sen) as new_sen_count
from land_date;

-- closing number of Server customers and SENs
with closing_SEN_Count as (
        select  *
        from public.license
        where platform = 'Server'
        and first_paid_license_level = 'Full'
        and paid_license_end_date > '2016-06-31'
        and base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
)
select  count(distinct tech_email_domain) as closing_cust_count,
        count(distinct sen) as closing_sen_count
from closing_SEN_Count;

--identify SENs that were there in opening, but not in closing

with opening_SEN_Count as (
        select  *
        from public.license
        where platform = 'Server'
        and first_paid_license_level = 'Full'
        and paid_license_end_date > '2016-03-31'
        and base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
),
closing_SEN_Count as (
        select  *
        from public.license
        where platform = 'Server'
        and first_paid_license_level = 'Full'
        and paid_license_end_date > '2016-06-30'
        and base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
)
select sen
from opening_SEN_Count
where sen not in (select sen from closing_SEN_Count)
;
-- identify LA Reasons for churn --summary -- have to switch to Greenzone
WITH churn_buckets AS
(
SELECT *, 
CASE 
WHEN stage_reason IN(
'Company Out of Business', 
'Consolidating Instances',
'Cross-graded to JIRA Core', 
'Downgrade to Starter',
'Using another instance (same product)',
'Moved to Cloud',
'JIRA Renaissance Package / Pricing',
'Moved to another BTF product',
'Moved to Data Center' ) THEN 'Non-regretted'
WHEN stage_reason IN(
'Don''t want to renew', 
'No longer use Product', 
'No need for Support / Updates',
'Too expensive / No budget',
'Using Competitor''s Product',
'Waiting to Update Product',
'Not satisfied with Support / Updates',
'Waiting to Upgrade User-Tiers',
'Outstanding support issues / feature requests',
'Not in Production' ) THEN 'Regretted'
WHEN stage_reason IN(
'Contact info unavailable' ,
'Invalid Contact Information',
'Invalid Opportunity',
'No Response',
'Reason not given',
'Unable to Contact', 
'Project Completed',
'Other/no additional information given',
'Company Out of Business',
'Other (see General Comments)' ) OR stage_reason is null THEN 'Exclude'
END AS churn_type
FROM playground_intsys.renewals_lost
WHERE lost_date >= '2015-07-01'
),
b AS (
SELECT sen,
CASE 
WHEN amount BETWEEN 0 and 499 THEN 'Less than $500'
WHEN amount BETWEEN 500 and 999 THEN 'Between $500 and $999'
WHEN amount BETWEEN 1000 and 4999 THEN 'Between $1k and $5k'
WHEN amount BETWEEN 5000 and 9999 THEN 'Between $5k and $10k'
WHEN amount >= 10000 THEN 'Greater than $10k' END AS value_bucket,
stage_reason
FROM churn_buckets
GROUP BY 1,2,3
ORDER BY 2)
SELECT stage_reason, count(sen)
FROM b
where sen in (
with opening_SEN_Count as (
        select  *
        from public.license
        where platform = 'Server'
        and license_level = 'Full'
        and expiry_date > '2016-03-31'
        and base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
),
closing_SEN_Count as (
        select  *
        from public.license
        where platform = 'Server'
        and license_level = 'Full'
        and expiry_date > '2016-06-30'
        and base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
)
select cast (sen as text)
from opening_SEN_Count
where sen not in (select sen from closing_SEN_Count)
)
group by 1;

--actual sen of loyalty advocate
WITH churn_buckets AS
(
SELECT *, 
CASE 
WHEN stage_reason IN(
'Company Out of Business', 
'Consolidating Instances',
'Cross-graded to JIRA Core', 
'Downgrade to Starter',
'Using another instance (same product)',
'Moved to Cloud',
'JIRA Renaissance Package / Pricing',
'Moved to another BTF product',
'Moved to Data Center' ) THEN 'Non-regretted'
WHEN stage_reason IN(
'Don''t want to renew', 
'No longer use Product', 
'No need for Support / Updates',
'Too expensive / No budget',
'Using Competitor''s Product',
'Waiting to Update Product',
'Not satisfied with Support / Updates',
'Waiting to Upgrade User-Tiers',
'Outstanding support issues / feature requests',
'Not in Production' ) THEN 'Regretted'
WHEN stage_reason IN(
'Contact info unavailable' ,
'Invalid Contact Information',
'Invalid Opportunity',
'No Response',
'Reason not given',
'Unable to Contact', 
'Project Completed',
'Other/no additional information given',
'Company Out of Business',
'Other (see General Comments)' ) OR stage_reason is null THEN 'Exclude'
END AS churn_type
FROM playground_intsys.renewals_lost
WHERE lost_date >= '2015-07-01'
),
b AS (
SELECT sen,
CASE 
WHEN amount BETWEEN 0 and 499 THEN 'Less than $500'
WHEN amount BETWEEN 500 and 999 THEN 'Between $500 and $999'
WHEN amount BETWEEN 1000 and 4999 THEN 'Between $1k and $5k'
WHEN amount BETWEEN 5000 and 9999 THEN 'Between $5k and $10k'
WHEN amount >= 10000 THEN 'Greater than $10k' END AS value_bucket,
stage_reason
FROM churn_buckets
GROUP BY 1,2,3
ORDER BY 2)
SELECT sen
FROM b
where sen in (
with opening_SEN_Count as (
        select  *
        from public.license
        where platform = 'Server'
        and license_level = 'Full'
        and expiry_date > '2016-03-31'
        and base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
),
closing_SEN_Count as (
        select  *
        from public.license
        where platform = 'Server'
        and license_level = 'Full'
        and expiry_date > '2016-06-30'
        and base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
)
select cast (sen as text)
from opening_SEN_Count
where sen not in (select sen from closing_SEN_Count)
)
group by 1;

--check downgrade query
-- Calculates SENs that can be attributed to downgrades.

with    
land_sen as (
        select  sen, 
                base_product,
                email_domain,
                quarter,
                financial_year,
                min(date) as min_date
        from    public.sale
        where   license_level= 'Full'
        and     platform = 'Server'
        and     base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and sen in (
with opening_SEN_Count as (
        select  *
        from public.license
        where platform = 'Server'
        and license_level = 'Full'
        and expiry_date > '2016-03-31'
        and base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
),
closing_SEN_Count as (
        select  *
        from public.license
        where platform = 'Server'
        and license_level = 'Full'
        and expiry_date > '2016-06-30'
        and base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
)
select cast (sen as text)
from opening_SEN_Count
where sen not in (select sen from closing_SEN_Count)
)
        group by 1,2,3,4,5
),
downgrade_samesen as (
        select  a.sen
        from    land_sen as a 
        left join public.sale as b on a.sen = b.sen
        where   b.platform = 'Server'
        and     b.license_level = 'Starter'
        and     b.date >= a.min_date
        group by 1
),
renewed as (
        select  a.sen,
                a.email_domain,
                b.quarter,
                max(b.date) as max_date
        from    land_sen as a 
        left join public.sale as b on a.sen = b.sen
        where   b.sale_type = 'Renewal'
        and     b.quarter > a.quarter
        group by 1,2,3
        order by 1
),
starter_land as (
        select  sen, 
                email_domain,
                base_product,
                min(date) as min_start
        from public.sale
        where   sale_type = 'Starter'
        and     platform = 'Server'
        group by 1,2,3
),
renewed_starter as (
        select  a.sen as affected_sen,
                a.email_domain
        from    land_sen as a
        left join starter_land as b on a.email_domain = b.email_domain
        left join renewed as c on a.sen = c.sen
        where   a.base_product = b.base_product
        and     b.min_start > c.max_date + interval '275' DAY
        and     b.min_start < c.max_date + interval '455' DAY
        and     a.quarter < c.quarter
        group by 1,2
        order by 1
),
non_renewed_starter as (
        select a.sen as affected_sen,
        a.email_domain
        from    land_sen as a
        left join starter_land as b on a.email_domain = b.email_domain
        where   a.base_product = b.base_product
        and     b.min_start > a.min_date + interval '275' DAY
        and     b.min_start < a.min_date + interval '455' DAY
        and     a.sen not in (select sen from renewed)
        group by 1,2
        order by 1,2
),
active_prod as (
        select  b.tech_email_domain,
                b.base_product,
                count(distinct b.sen) as prod_count
        from    land_sen as a
        left join public.license as b on a.email_domain = b.tech_email_domain
        left join starter_land as c on a.email_domain = c.email_domain and a.base_product = c.base_product
        where   b.last_license_level = 'Full'
        and     cast(b.paid_license_end_date as date) < c.min_start
        and     cast(b.paid_license_end_date as date) > c.min_start - interval '90' DAY
        group by 1,2
),
final_sen_downgrade_new as (
        select  distinct a.affected_sen
        from    renewed_starter as a
        join    active_prod as c on a.email_domain = c.tech_email_domain  
        and     a.affected_sen not in (select sen from downgrade_samesen)
        and     c.prod_count = 1

union
        
        select  distinct b.affected_sen
        from    non_renewed_starter as b
        join    active_prod as c on b.email_domain = c.tech_email_domain  
        and     b.affected_sen not in (select sen from downgrade_samesen)
        and     c.prod_count = 1
        group by 1
        order by 1
)
        select  a.affected_sen as sen
        from    final_sen_downgrade_new as a
        left join land_sen as b on a.affected_sen=b.sen
        group by 1
        
        union
        
        select a.sen as sen
        from downgrade_samesen as a
        left join land_sen as b on a.sen = b.sen
        group by 1;


--sens for consolidation
with    
land_sen as (
        select  sen, 
                base_product,
                email_domain,
                quarter,
                financial_year,
                min(date) as min_date
        from    public.sale
        where   license_level= 'Full'
        and     platform = 'Server'
        and     base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and     financial_year > 'FY2010'
        and     financial_year <> 'FY2017'
        group by 1,2,3,4,5
),
renewed as (
        select  a.sen,
                a.email_domain,
                b.quarter,
                max(b.date) as max_date
        from    land_sen as a 
        left join public.sale as b on a.sen = b.sen
        where   b.sale_type = 'Renewal'
        and     b.quarter > a.quarter
        group by 1,2,3
        order by 1
),
land_churn_date as (
        select  sen, 
                (min_date + 545) as churn_date
        from land_sen
        where sen not in (select sen from renewed) 
),
renewed_churn_date as (
        select  sen, 
                (max_date + 545) as churn_date
        from    renewed
)
,non_renewed_prod_count as (      
        select  a.sen,
                a.email_domain,
                b.base_product,
                count(distinct b.sen) as prod_count
        from land_sen as a
        left join public.license as b on a.email_domain = b.tech_email_domain
        left join land_churn_date as c on a.sen = c.sen
        where a.sen in (select sen from land_churn_date)
        and b.start_date between a.min_date and c.churn_date
        and a.base_product = b.base_product
        and b.platform = 'Server'
        group by 1,2,3
)      
,renewed_prod_count as (      
        select  a.sen,
                a.email_domain,
                b.base_product,
                count(distinct b.sen) as prod_count
        from land_sen as a
        left join public.license as b on a.email_domain = b.tech_email_domain
        left join renewed_churn_date as c on a.sen = c.sen
        where a.sen in (select sen from renewed_churn_date)
        and b.start_date between a.min_date and c.churn_date
        and a.base_product = b.base_product
        and b.platform = 'Server'
        group by 1,2,3      
),
final_multi_sen_list as (
select  a.financial_year, 
        a.sen
        from land_sen as a
        left join non_renewed_prod_count as b on a.sen = b.sen
        where b.prod_count > 1
        
union 

select  a.financial_year, 
        a.sen
        from land_sen as a
        left join renewed_prod_count as b on a.sen = b.sen
        where b.prod_count > 1
)
select  a.sen
from final_multi_sen_list as a
left join license as b on a.sen = cast(b.sen as text)
where b.expiry_date <= '2016-06-30'
and a.sen in 
(
with opening_SEN_Count as (
        select  *
        from public.license
        where platform = 'Server'
        and license_level = 'Full'
        and expiry_date > '2016-03-31'
        and base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
),
closing_SEN_Count as (
        select  *
        from public.license
        where platform = 'Server'
        and license_level = 'Full'
        and expiry_date > '2016-06-30'
        and base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
)
select cast (sen as text)
from opening_SEN_Count
)
-- check consolidation query and remove downgrades
with    
land_sen as (
        select  sen, 
                base_product,
                email_domain,
                quarter,
                financial_year,
                min(date) as min_date
        from    public.sale
        where   license_level= 'Full'
        and     platform = 'Server'
        and     base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and     financial_year > 'FY2010'
        and     financial_year <> 'FY2017'
        group by 1,2,3,4,5
),
renewed as (
        select  a.sen,
                a.email_domain,
                b.quarter,
                max(b.date) as max_date
        from    land_sen as a 
        left join public.sale as b on a.sen = b.sen
        where   b.sale_type = 'Renewal'
        and     b.quarter > a.quarter
        group by 1,2,3
        order by 1
),
land_churn_date as (
        select  sen, 
                (min_date + 545) as churn_date
        from land_sen
        where sen not in (select sen from renewed) 
),
renewed_churn_date as (
        select  sen, 
                (max_date + 545) as churn_date
        from    renewed
)
,non_renewed_prod_count as (      
        select  a.sen,
                a.email_domain,
                b.base_product,
                count(distinct b.sen) as prod_count
        from land_sen as a
        left join public.license as b on a.email_domain = b.tech_email_domain
        left join land_churn_date as c on a.sen = c.sen
        where a.sen in (select sen from land_churn_date)
        and b.start_date between a.min_date and c.churn_date
        and a.base_product = b.base_product
        and b.platform = 'Server'
        group by 1,2,3
)      
,renewed_prod_count as (      
        select  a.sen,
                a.email_domain,
                b.base_product,
                count(distinct b.sen) as prod_count
        from land_sen as a
        left join public.license as b on a.email_domain = b.tech_email_domain
        left join renewed_churn_date as c on a.sen = c.sen
        where a.sen in (select sen from renewed_churn_date)
        and b.start_date between a.min_date and c.churn_date
        and a.base_product = b.base_product
        and b.platform = 'Server'
        group by 1,2,3      
),
final_multi_sen_list as (
select  a.financial_year, 
        a.sen
        from land_sen as a
        left join non_renewed_prod_count as b on a.sen = b.sen
        where b.prod_count > 1
        
union 

select  a.financial_year, 
        a.sen
        from land_sen as a
        left join renewed_prod_count as b on a.sen = b.sen
        where b.prod_count > 1
)
select  a.financial_year,  
        count(distinct a.sen)
from final_multi_sen_list as a
left join license as b on a.sen = cast(b.sen as text)
where b.expiry_date <= '2016-06-30'
and a.sen in 
(
with opening_SEN_Count as (
        select  *
        from public.license
        where platform = 'Server'
        and license_level = 'Full'
        and expiry_date > '2016-03-31'
        and base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
),
closing_SEN_Count as (
        select  *
        from public.license
        where platform = 'Server'
        and license_level = 'Full'
        and expiry_date > '2016-06-30'
        and base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
)
select cast (sen as text)
from opening_SEN_Count
where sen not in (select sen from closing_SEN_Count)
)
and a.sen not in (
-- Calculates SENs that can be attributed to downgrades.

with    
land_sen as (
        select  sen, 
                base_product,
                email_domain,
                quarter,
                financial_year,
                min(date) as min_date
        from    public.sale
        where   license_level= 'Full'
        and     platform = 'Server'
        and     base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and     financial_year > 'FY2010'
        group by 1,2,3,4,5
),
downgrade_samesen as (
        select  a.sen
        from    land_sen as a 
        left join public.sale as b on a.sen = b.sen
        where   b.platform = 'Server'
        and     b.license_level = 'Starter'
        and     b.date >= a.min_date
        group by 1
),
renewed as (
        select  a.sen,
                a.email_domain,
                b.quarter,
                max(b.date) as max_date
        from    land_sen as a 
        left join public.sale as b on a.sen = b.sen
        where   b.sale_type = 'Renewal'
        and     b.quarter > a.quarter
        group by 1,2,3
        order by 1
),
starter_land as (
        select  sen, 
                email_domain,
                base_product,
                min(date) as min_start
        from public.sale
        where   sale_type = 'Starter'
        and     platform = 'Server'
        group by 1,2,3
),
renewed_starter as (
        select  a.sen as affected_sen,
                a.email_domain
        from    land_sen as a
        left join starter_land as b on a.email_domain = b.email_domain
        left join renewed as c on a.sen = c.sen
        where   a.base_product = b.base_product
        and     b.min_start > c.max_date + interval '275' DAY
        and     b.min_start < c.max_date + interval '455' DAY
        and     a.quarter < c.quarter
        group by 1,2
        order by 1
),
non_renewed_starter as (
        select a.sen as affected_sen,
        a.email_domain
        from    land_sen as a
        left join starter_land as b on a.email_domain = b.email_domain
        where   a.base_product = b.base_product
        and     b.min_start > a.min_date + interval '275' DAY
        and     b.min_start < a.min_date + interval '455' DAY
        and     a.sen not in (select sen from renewed)
        group by 1,2
        order by 1,2
),
active_prod as (
        select  b.tech_email_domain,
                b.base_product,
                count(distinct b.sen) as prod_count
        from    land_sen as a
        left join public.license as b on a.email_domain = b.tech_email_domain
        left join starter_land as c on a.email_domain = c.email_domain and a.base_product = c.base_product
        where   b.last_license_level = 'Full'
        and     cast(b.paid_license_end_date as date) < c.min_start
        and     cast(b.paid_license_end_date as date) > c.min_start - interval '90' DAY
        group by 1,2
),
final_sen_downgrade_new as (
        select  distinct a.affected_sen
        from    renewed_starter as a
        join    active_prod as c on a.email_domain = c.tech_email_domain  
        and     a.affected_sen not in (select sen from downgrade_samesen)
        and     c.prod_count = 1

union
        
        select  distinct b.affected_sen
        from    non_renewed_starter as b
        join    active_prod as c on b.email_domain = c.tech_email_domain  
        and     b.affected_sen not in (select sen from downgrade_samesen)
        and     c.prod_count = 1
        group by 1
        order by 1
)
        select  a.affected_sen as sen
        from    final_sen_downgrade_new as a
        left join land_sen as b on a.affected_sen=b.sen
        group by 1
        
        union
        
        select a.sen as sen
        from downgrade_samesen as a
        left join land_sen as b on a.sen = b.sen
        group by 1

)
group by 1